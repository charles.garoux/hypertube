const User = require('../Schemas/user.js');
const CommonFunctions = require('../Library/CommonFunctions');

module.exports = class Comments extends CommonFunctions {

  static async getComments(imdbid)
  {
      return User.find({'comments.imdb': imdbid}).then((users) => {
        var com = [];
        var i = 0;
        while (i < users.length) {
          var j = 0;
          while (j < users[i].comments.length) {
            if (users[i].comments[j].imdb === imdbid) {
              com.push({username: users[i].username, text: users[i].comments[j].text, date: users[i].comments[j].date, picture: users[i].picture});
            }
            j++;
          }
          i++;
        }
        var sortCom = com.sort(function(a, b){return b.date - a.date});
        return sortCom;
      });
  }

  static async addComment(text, imdbid, uid)
  {
    let resPromise = new Promise((resolve, reject) => {
        User.findOne({_id: uid}).then((user) => {
            if (user) {
                user.comments.push({imdb: CommonFunctions._escapeHtml(imdbid), text: CommonFunctions._escapeHtml(text), date: Date.now() + 3600000});
                user.save(function(err, update) {
                  if (err)
                    resolve ({err: "Error"});
                  else
                    resolve ({data: {username: user.username, picture: user.picture, text: update.comments[update.comments.length - 1].text, date: update.comments[update.comments.length - 1].date}});
                });
            }
            else
              resolve ({err: "Error user not found"});
        });
    });
    return resPromise;
  }

}
