const CacheEngine = require('../Library/CacheEngine');
const User = require('../Schemas/user.js');

module.exports = class Home {
    static listWithViewed(uid, callback)
    {
        CacheEngine.retrievePopularVideoList()
        .then((list) => {
            list = list.toObject()
            User.find({_id: uid}, {views: 1}, (err, res) => {
                list = list.map((movie) => {
                    movie.viewed = false;
                    res[0].views.forEach((v) => { 
                        if (v.imdb === movie.imdbid)
                            movie.viewed = true;
                    });
                    return movie;
                })
                callback(list)
            })
        })
        .catch((err) => {
          //console.log(err)
        })
    }
}