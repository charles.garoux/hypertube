const nodemailer = require('nodemailer');
const User = require('../Schemas/user.js');
const passwordHash = require('password-hash');
const colors = require('colors/safe');
const CommonFunctions = require('../Library/CommonFunctions');
const fs = require('fs')
const servConfig = JSON.parse(fs.readFileSync('./config/server.config.json'))

var transporter = nodemailer.createTransport(servConfig.email);

module.exports = class Register extends CommonFunctions{

  static login(username, password)
  {
    return User.findOne({username: username.toLowerCase()}).then((user) => {
      if (user) {
        var verif = passwordHash.verify(password, user.pass);
        if (verif) {
          if (user.access === true)
            return {code: 0, error: 'Ok', user: user};
          else
            return {code: 1, error: "Your must confirm your mail before you can login"};
        }
        else
          return {code: 2, error: "Bad password"};
      }
      else
        return {code: 3, error: "User does not exist"};
    });
  }

  static register(firstname, lastname, email, username, pass, cpass, callback)
  {
    User.findOne({mail: email.toLowerCase()}).then((checkMail) => {
      if (checkMail)
        callback({code: 1, error: "Already existing email"});
      else {
          User.findOne({username: username.toLowerCase()}).then((checkUsername) => {
            if (checkUsername)
              callback({code: 2, error: "Already existing username"});
            else {
              if (pass === cpass) {
                User.find({}, function(err, users) {
                  var userMap = [];
                  var i = 0;
                  users.forEach(function(user) {
                    userMap[i] = user;
                    i++;
                  });
                  while(1) {
                    var j = 0;
                    var tabL = userMap.length;
                    var token = Math.random().toString(36).substring(2);
                    var count = 0;
                    while (j < tabL) {
                      if (token === userMap[j].token){
                        count++;
                        break;
                      }
                      j++;
                    }
                    if (count === 0) {
                      new User({
                        username: CommonFunctions._escapeHtml(username.toLowerCase()),
                        firstname: CommonFunctions._escapeHtml(firstname),
                        lastname: CommonFunctions._escapeHtml(lastname),
                        mail: CommonFunctions._escapeHtml(email.toLowerCase()),
                        pass: passwordHash.generate(pass),
                        lang: "en",
                        access: false,
                        token: token
                      }).save().then((newUser) => {
                        var mailOptions = {
                                from: 'Hypertube',
                                to: newUser.mail,
                                subject: 'Hypertube - Confirm your registration',
                                html: `<html><a href="http://${servConfig.server.adress}:${servConfig.server.port}/confirm/${newUser.token}">http://${servConfig.server.adress}:${servConfig.server.port}/confirm/${newUser.token}</a></html>`
                        };
                        transporter.sendMail(mailOptions, function(error, info){
                            if(error){
                               //console.log(error);
                               return callback({code: 4, error: error})
                            }
                            console.log('Confirm registration mail send to: ' + colors.yellow(newUser.mail));
                        });
                        transporter.close();
                        console.log('New user created: ' + colors.yellow(newUser.username) + ' with register form');
                        callback({code:0 ,confirm: "Thanks for the register, an email has been sent to you to confirm the registration! This confirmation is required to access the site."});
                      });
                      break;
                    }
                  }
                });
              }
              else
                callback({code: 7, error: "Passwords do not match"});
            }
          });
      }
    });
  }

  static forgotPassword(email, callback)
  {
    if (email) {
        User.findOne({mail: email.toLowerCase()}).then((user) => {
          if (user) {
            User.find({}, function(err, users) {
              var userMap = [];
              var i = 0;
              users.forEach(function(user) {
                userMap[i] = user;
                i++;
              });
              while(1) {
                var j = 0;
                var tabL = userMap.length;
                var token = Math.random().toString(36).substring(2);
                var count = 0;
                while (j < tabL) {
                  if (token === userMap[j].token){
                    count++;
                    break;
                  }
                  j++;
                }
                if (count === 0) {
                  User.findOne({mail: email.toLowerCase()}).then((user) => {
                    if (user) {
                      user.token = token;
                      user.save(function(err, update) {
                        if (err)
                        {
                           //console.log(err);
                        }
                        else {
                          var mailOptions = {
                                  from: 'Hypertube',
                                  to: update.mail,
                                  subject: 'Hypertube - Reset your password',
                                  html: `<html><a href="http://${servConfig.server.adress}:${servConfig.server.port}/reset/${update.token}">http://${servConfig.server.adress}:${servConfig.server.port}/reset/${update.token}</a></html>`
                          };
                          transporter.sendMail(mailOptions, function(error, info){
                              if(error)
                                return;
                                // console.log(error);
                              console.log('Reinitilization email send to: ' + colors.yellow(update.mail));
                          });
                          transporter.close();
                          callback({code: 0, confirm: "Reinitilization email send"});
                        }
                      });
                    }
                    else
                      callback({code: 1, error: "This email address is not linked to any account"});
                  });
                  break;
                }
              }
            });
          }
          else
            callback({code: 2, error: "This email address is not linked to any account"});
        });
      }
      else
        callback({code: 3, error: "Error data"});
  }

  static resetPassword(pass, cpass, token, callback)
  {
    User.findOne({token: token}).then((user) => {
      if (user) {
        if (pass === cpass) {
          user.pass = passwordHash.generate(pass);
          user.token = '';
          user.save(function(err, update) {
            if (err)
            {
              //console.log(err);
            }
            else {
              console.log('Password refind with success for: ' + colors.yellow(update.mail));
              callback({code: 0, confirm: "Changed password you can now login"});
            }
          });
        }
        else
          callback({code: 3, error: "Passwords do not match"});
      }
      else
        callback({code: 4, error: 404});
    });
  }

  static confirmMail(token, callback)
  {
    User.findOne({token: token}).then((user) => {
        if (user) {
          user.access = true;
          user.token = '';
          user.save(function(err, update) {
            if (err)
            {
              //console.log(err);
            }
            else
              callback({code: 0, confirm: "Mail confirm you can now log in"});
          });
        }
        else
          callback({code: 1, error: 404})
    });
  }

  static prepareResetPassword(token, callback)
  {
    User.findOne({token: token}).then((user) => {
      if (user)
        callback({code: 0});
      else
        callback({code: 1, error: 404});
    });
  }

}
