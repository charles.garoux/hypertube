const colors = require('colors/safe');
const PirateBay = require('thepiratebay');
const imdb = require('imdb-api')
const ptt = require("parse-torrent-title");
const TorrentSearchApi = require('torrent-search-api');
TorrentSearchApi.enableProvider('1337x');
const fs = require('fs');
const servConfig = JSON.parse(fs.readFileSync('./config/server.config.json'));
const User = require('../Schemas/user.js');

const ImdbCollector = require('../Library/ImdbCollector');
const Video = require('../Library/Video.class');

module.exports = class Search {


  static _addViewed(uid, list, callback)
  {
    //console.log('LIST', list)
      User.find({_id: uid}, {views: 1}, (err, res) => {
          //console.log(res[0])
          list = list.map((movie) => {
              movie.viewed = false;
              res[0].views.forEach((v) => { 
                  if (v.imdb === movie.imdbid)
                      movie.viewed = true;
              });
              return movie;
          })
          callback(list)
      })
  }

  static async getResults(query, uid)
  {
    const imdbDataList = await ImdbCollector.listByTitle(query);

    let videoList = imdbDataList.map(imdbData => {
      return new Video(imdbData);
    });

    let promiseRes = new Promise((resolve, reject) => {
      this._addViewed(uid, videoList, (res) => {
        resolve (res);
      })
    });

    return promiseRes;
  }
};
