const colors = require('colors/safe');
const fs = require("fs");
const User = require('../Schemas/user.js');
const passwordHash = require('password-hash');
const CommonFunctions = require('../Library/CommonFunctions');

module.exports = class Settings extends CommonFunctions {

  static changePasswordSettings(pass, cpass, user, callback)
  {
    if (pass === cpass) {
      User.findOne({_id: user._id}).then((user) => {
        if (user) {
          user.pass = passwordHash.generate(pass);
          user.save(function(err, update) {
            if (err)
            {
              //console.log(err);
            }
            else {
              console.log(colors.yellow(update.username) + " have a new password");
              callback({code: 0, confirm: 'Password modify with success.', user: update})
            }
          });
        }
        else
          callback({code: 404, error: 404});
      });
    }
    else {
      callback({code: 1, error: "Password do not match."})
    }
  }

  static changeSettings(lang, username, firstname, lastname, email, user, callback)
  {
    User.findOne({mail: email.toLowerCase()}).then((checkMail) => {
      if (checkMail && checkMail.mail !== user.mail)
        callback({code: 1, error: "Already existing email."})
      else {
        User.findOne({username: username.toLowerCase()}).then((checkUser) => {
          if (checkUser && checkUser.username !== user.username)
            callback({code: 1, error: "Already existing username."})
          else {
            User.findOne({_id: user._id}).then((user) => {
              if (user) {
                user.lang = lang;
                user.mail = CommonFunctions._escapeHtml(email.toLowerCase());
                user.username = CommonFunctions._escapeHtml(username.toLowerCase());
                user.firstname = CommonFunctions._escapeHtml(firstname);
                user.lastname = CommonFunctions._escapeHtml(lastname);
                user.save(function(err, update) {
                  if (err)
                  {
                    //console.log(err);
                  }
                  else {
                    console.log(colors.yellow(update.username) + " updated his info");
                    callback({code: 0, confirm: 'Updated with success.', user: update});
                  }
                });
              }
              else {
                callback({code: 404, error: 404});
              }
            });
          }
        });
      }
    });
  }

  static changeOauthSettings(lang, username, firstname, lastname, user, callback)
  {
    User.findOne({username: username.toLowerCase()}).then((checkUser) => {
      if (checkUser && checkUser.username !== user.username)
        callback({code: 1, error: "Error already existing username"})
      else {
        User.findOne({_id: user._id}).then((user) => {
          if (user) {
            user.lang = lang;
            user.username = CommonFunctions._escapeHtml(username.toLowerCase());
            user.firstname = CommonFunctions._escapeHtml(firstname);
            user.lastname = CommonFunctions._escapeHtml(lastname);
            user.save(function(err, update) {
              if (err)
              {
                //console.log(err);
              }
              else {
                console.log(colors.yellow(update.username) + " updated his info");
                callback({code: 0, confirm: 'Updated with success.', user: update})
              }
            });
          }
          else
            callback({code: 404, error: 404});
        });
      }
    });
  }

  static changePictureSettings(base64, user, callback)
  {
    var formatCheck = base64.split(',');
    if (formatCheck[0] == "data:image/png;base64") {
      var format = ".png";
      var base64Data = base64.replace(/^data:image\/png;base64,/, "");
    } else {
      var format = ".jpg";
      var base64Data = base64.replace(/^data:image\/jpeg;base64,/, "");
    }
    var imgSrc = "./public/upload/" + user._id + format;
    var imgLink = "/upload/" + user._id + format;
    fs.writeFile(imgSrc, base64Data, 'base64', function(err) {});
    User.findOne({_id: user._id}).then((user) => {
      if (user) {
        user.picture = imgLink;
        user.save(function(err, update) {
          if (err)
          {
            //console.log(err);
          }
          else {
            console.log(colors.yellow(update.username) + " change profile picture");
            callback({code: 0, confirm: "Picture modify with success.", user: update})
          }
        });
      }
      else
        callback({code: 404, error: 404})
    });
  }

}
