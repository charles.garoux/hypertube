const User = require('../Schemas/user.js');
const imdb = require('imdb-api');
const Torrent = require('../Schemas/torrent.js');
const fs = require('fs')
const servConfig = JSON.parse(fs.readFileSync('./config/server.config.json'))
const TorrentCollector = require('../Library/TorrentCollector');
const ImdbCollector = require('../Library/ImdbCollector');

module.exports = class Video {

  static async getMovieInfo(imdbid, uid)
  {
      let result = await ImdbCollector.getByid(imdbid);
      return (result);
  }

  static setMovieSeen(imdbid, uid) {
    try {
      imdb.get({id: imdbid}, {apiKey: servConfig.imdb.apiKey})
      .then(result => {
        //console.log(result.title)
        User.updateOne({ _id: uid, 'views.imdb': { $ne: imdbid } }, { $push: { views: {imdb: imdbid, title: result.title} } }, (e, r) => {
          //console.log(e,r.nModified)
        });
        User.updateOne({ _id: uid, 'views.imdb':  imdbid }, {'views.$.date': Date.now()}, (e, r) => {
          //console.log(e,r.nModified)
        });
      })
      .catch(err => {
          //console.log (err)
      });
    } catch (err) {
      //console.log(err)
    }
  }

  static async getMagnetList(imdbid, title, year, season, callback)
  {
    TorrentCollector.searchTorrents(title, year, imdbid, season, 100).then((sortVideolist) => {
      Video._isReady(sortVideolist, (vList) => {
        callback(vList);
      })
    })
  }

  static _isReady(vList, callback)
  {
      Torrent.find({$or: [
                          {$and: [
                            {'files.status': 'downloaded'},
                            {'files.vCodec': 'h264'}
                          ]},
                          {$and: [
                            {'files.status': 'converted'},
                            {'files.vCodec': {$ne: 'h264'}}
                          ]}
                      ]}).then((torrents) => {
          vList.forEach((v) => {
            v.ready = false;
            let tid = v.tid
            torrents.forEach((t) => {
                if (t.tid === tid)
                v.ready = true;
            })
          })
          callback(vList)
      })
  }
}