const TorrentCollector = require('./TorrentCollector');
const ImdbCollector = require('./ImdbCollector');
const PopularVideoList = require('../Schemas/PopularVideoListModel');
const fs = require('fs')
const servConfig = JSON.parse(fs.readFileSync('./config/server.config.json'))

let CacheEngine = {

    nbrToCache : servConfig.cache.nbrToCache,

    refresh : async function() {
        let popularTorrentList = await TorrentCollector.getPopular(this.nbrToCache);

        /**
         * Launch promise on all Torrent to set data from imdb
         */
        let promiseList = [];
        for (const Torrent of popularTorrentList) {
            const promise = new Promise(((resolve, reject) => {
                let query = {name: Torrent.title};
                if (Torrent.year !== null)
                    query.year = Torrent.year;

                ImdbCollector.get(query)
                    .then(result => {
                        Torrent.addImdbData(result);
                        resolve(Torrent);
                    })
                    .catch(error => {
                        resolve();
                    });
            }));
            promiseList = promiseList.concat(promise);
        }
        let popularImdbVideoList = await Promise.all(promiseList);

        popularImdbVideoList = this._cleanVideoList(popularImdbVideoList);

        popularImdbVideoList.sort(((a, b) => {
            return (b.seed - a.seed);
        }));

        const newList = await PopularVideoList.create({popularVideoList: popularImdbVideoList});
        await PopularVideoList.deleteMany({'timestampOfCreation': {$lt: newList.timestampOfCreation}});

        return (popularImdbVideoList);
    },

    _cleanVideoList : function(videoList) {
        let cleanedList = [];

        /**
         * Delete video that does not exist (not found on imdb)
         */
        videoList = videoList.filter(object => {
            return (object !== undefined);
        });

        /**
         * Delete video without poster
         */
        videoList = videoList.filter(object => {
            return (object.poster !== 'N/A');
        });

        videoList = videoList.filter(object => {
            return (object.seed !== 0);
        });

        for (const element of videoList) {
            let copyList = videoList.filter(object => {
                return object.imdbid === element.imdbid
            });
            if (copyList.length > 1) {
                let videoUnique = copyList[0].clone();

                for (const video of copyList) {
                    if (video.seed > videoUnique.seed)
                        videoUnique.seed = video.seed;
                    videoList.splice(videoList.indexOf(video), 1)
                }
                cleanedList = cleanedList.concat(videoUnique);
            }
            else
                cleanedList = cleanedList.concat(copyList);
        }

        return (cleanedList);
    },

    retrievePopularVideoList : async function () {
        let result = await PopularVideoList.find().sort({timestampOfCreation: -1}).limit(1);
        return (result[0].popularVideoList);
    },
};

module.exports = CacheEngine;
