module.exports = class Crons {
    constructor() {
        this.crons = {};
    }

    addCron(name, callback, interval)
    {
        callback();
        this.crons[name] = setInterval(callback, interval);
    }

    stopCron(name)
    {
        clearInterval(this.crons[name]);
    }
}