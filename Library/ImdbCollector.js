const imdbApi = require('imdb-api')
const fs = require('fs')
const servConfig = JSON.parse(fs.readFileSync('./config/server.config.json'))

let ImdbCollector = {

    apiKey : servConfig.imdb.apiKey,
    timeout : 30000,

    /**
     * Methods name format :
     *  get = 1 result
     *  list = results in list
     */
    
    get : async function(request) {
        const result = await imdbApi.get(request, {apiKey: this.apiKey, timeout: this.timeout});

        return (result);
    },


    getByid : async function(imdbid) {
        const result = await imdbApi.get({id: imdbid}, {apiKey: this.apiKey, timeout: this.timeout});

        let videoData = {
            title: result.title,
            plot: result.plot,
            score: result.rating,
            poster: result.poster,
            actors: result.actors,
            writer: result.writer,
            director: result.director,
            runtime: result.runtime,
            year: result.year || result.start_year,
            totalseasons: result.totalseasons,
            series: result.series
        };

        return (videoData);
    },

    listByTitle : async function(title) {
        const result = await imdbApi.search({name: title}, {apiKey: this.apiKey, timeout: this.timeout});

        let promiseList = [];
        for (const imdbData of result.results) {
            const promise = new Promise(((resolve, reject) => {
                this.get({name: imdbData.title })
                    .then(result => {
                        imdbData.score = result.rating;
                        resolve(imdbData);
                    })
                    .catch(error => {
                        resolve();
                    });
            }));
            promiseList = promiseList.concat(promise);
        }
        let promisesResults = await Promise.all(promiseList);

        let imdbDataList = this._cleanImdbList(promisesResults);

        imdbDataList = imdbDataList.sort((a, b) => {
            return a.title.localeCompare(b.title);
        })

        return (imdbDataList);
    },

    _cleanImdbList : function(imdbDataList) {
        let cleanedList = [];

        imdbDataList = imdbDataList.filter(object => {
            return (object !== undefined);
        });

        /**
         * Delete duplicate torrents
         */
        for (const element of imdbDataList) {
            let result = imdbDataList.filter(object => {
                return object.imdbid === element.imdbid
            });
            if (result.length > 1) {
                let torrentUnique = element;
                for (const torrent of result) {
                    if (torrent.seed > torrentUnique.seed)
                        torrentUnique.seed = torrent.seed;
                    imdbDataList.splice(imdbDataList.indexOf(torrent), 1)
                }
                cleanedList = cleanedList.concat(result[0]);
            }
            else
                cleanedList = cleanedList.concat(result);
        }

        return (cleanedList);
    }

};

module.exports = ImdbCollector;
