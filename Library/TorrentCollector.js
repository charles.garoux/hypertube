const PirateBay = require('thepiratebay');
const ParseTorrentTitle = require('parse-torrent-title');
const Eztv = require('eztv-b');
const eztv = new Eztv()
const parseTorrent = require('parse-torrent')
const Video = require('./Video.class');
const TorrentSearchApi = require('torrent-search-api');

const providers = ['Rarbg', 'ExtraTorrent', '1337x'];
providers.forEach((p) => {
    TorrentSearchApi.enableProvider(p)
})

let TorrentCollector = {
    nbrTorrentsPerSources : 1000,

    _formatBytes : function(bytes) {
        var marker = 1024;
        var decimal = 2;
        var kiloBytes = marker;
        var megaBytes = marker * marker;
        var gigaBytes = marker * marker * marker;
        var teraBytes = marker * marker * marker * marker;
  
        if(bytes < kiloBytes) return bytes + " Bytes";
        else if(bytes < megaBytes) return(bytes / kiloBytes).toFixed(decimal) + " KB";
        else if(bytes < gigaBytes) return(bytes / megaBytes).toFixed(decimal) + " MiB";
        else return(bytes / gigaBytes).toFixed(decimal) + " GiB";
    },
  

    _addSource : function(torrents, sourceName)
    {
        return torrents.map((t) => { 
            t.source = sourceName
            return t
        });
    },

    _getMagnets : async function(torrentList) {
        let promiseList = [];
        for (const torrent of torrentList) {
            const promise = new Promise(((resolve, reject) => {
                /**
                 * The query need a timeout, sometime the API don't respond
                 */
                const timeoutId = setTimeout(() => {
                    resolve();
                }, (torrentList.length * 0.01) * 1000);

                TorrentSearchApi.getMagnet(torrent)
                    .then(result => {
                        clearTimeout(timeoutId);
                        torrent.magnet = result;
                        resolve(torrent);
                    })
                    .catch(error => {
                        clearTimeout(timeoutId);
                        resolve();
                    });
            }));
            promiseList = promiseList.concat(promise);
        }
        let promisesResults = await Promise.all(promiseList);

        /**
         * Some result are "undefine"
         */
        promisesResults = promisesResults.filter(object => {
            return (object !== undefined);
        });

        return promisesResults;
    },

    _searchFromRarbg : async function(query, nbrTorrents = this.nbrTorrentsPerSources){
        let torrentList;

        try {
            torrentList = await TorrentSearchApi.search(['Rarbg'], query, 'Movies', nbrTorrents);
            torrentList = this._addSource(torrentList, 'Rarbg');
        } catch (err) {}

        torrentList = await this._getMagnets(torrentList);
        torrentList = this._addSource(torrentList, 'Rarbg');
        //console.log('Rarbg', torrentList.length)

        return (torrentList);
    },

    _searchFromExtraTorrent : async function(query, nbrTorrents = this.nbrTorrentsPerSources){
        let torrentList = await TorrentSearchApi.search(['ExtraTorrent'], query, 'Movies', nbrTorrents);

        torrentList = this._addSource(torrentList, 'ExtraTorrent');

        return (torrentList);
    },

    _searchFrom1337x : async function(query, nbrTorrents = this.nbrTorrentsPerSources){
        let torrentList = await TorrentSearchApi.search(['1337x'], query, 'Movies', nbrTorrents);

        torrentList = await this._getMagnets(torrentList);
        torrentList = this._addSource(torrentList, '1337x');
        //console.log('1337x', torrentList.length)
        return (torrentList);
    },

    /**
     * (This method does not use nbrTorrentsPerSources, 100 torrents max)
     * @param nbrTorrents
     * @returns {Promise<*>}
     */
    _getPopularFromThePirateBay : async function(nbrTorrents = this.nbrTorrentsPerSources) {
        const torrentList = await PirateBay.topTorrents(200);
        if (nbrTorrents < torrentList.length)
            torrentList.length = nbrTorrents;

        return (torrentList);
    },

    _searchFromThePirateBay : async function(query, limit) {
        let torrentPromise = new Promise(async (resolve, reject) => {
            setTimeout(() => {
                resolve([]);
            }, 6000);

            let torrentList = await PirateBay.search(query, {
                category: 200,
                orderBy: 'seeds',
                sortBy: 'desc'
              });
    
            torrentList.sort((a, b) => {
                return b.seeders - a.seeders
            })
    
            if (limit < torrentList.length)
                torrentList.length = limit;
    
            torrentList = this._addSource(torrentList, 'ThePirateBay');
            //console.log('TBP', torrentList.length)
            resolve (torrentList);
        });
        return torrentPromise;
    },

    /**
     * @param nbrTorrents
     * @returns {Promise<*>}
     */
    _getPopularFromEztu : async function(nbrTorrents = this.nbrTorrentsPerSources) {
        let torrentList = [];

        /**
         * Request all the necessary pages for the number of torrents requested
         */
        const nbrPage = nbrTorrents / 100;
        let page = 1;
        let promiseList = [];
        while (page <= nbrPage) {
            const promise = new Promise(((resolve, reject) => {
                eztv.getTorrents({page: page,limit : 100})
                    .then(result => { resolve(result) })
                    .catch(error => { resolve() });
            }));
            promiseList = promiseList.concat(promise);
            page++;
        }
        let promisesResults = await Promise.all(promiseList);

        /**
         * Clean result
         */
        promisesResults = promisesResults.filter(object => {
            return (object !== undefined);
        });

        /**
         * Concatenation of torrents arrays
         */
        for (const page of promisesResults) {
            torrentList = torrentList.concat(page.torrents);
        }

        return (torrentList.torrents);
    },


    _searchFromEztu : async function(imdbid, season = undefined) {
        let counter = 0;
        let page = 1;

        if (imdbid === '')
            return;

        let torrentList = [];
        imdbid = imdbid.substr(2)

        let promise1 = await eztv.getTorrents({imdb_id: imdbid})
        counter = promise1.torrents_count;
        if (counter === 0)
            return;

        if (season)
        {
            let promiseList = [];
            while (counter >= 0)
            {
                const promise = new Promise(((resolve, reject) => {
                    eztv.getTorrents({imdb_id: imdbid, page: page})
                        .then(result => { resolve(result) })
                        .catch(error => { resolve() });
                }));
                promiseList = promiseList.concat(promise);
                page++;
                counter -= promise1.torrents.length
            }
            promisesResults = await Promise.all(promiseList);

            for (const page of promisesResults) {
                torrentList = torrentList.concat(page.torrents);
            }

            torrentList = torrentList.filter((t) => {
                return t.season == season.substr(1);
            });
        }
        else
            torrentList = promise1.torrents

        torrentList = this._addSource(torrentList, 'EzTv');
        //console.log('EzTv', torrentList.length)
        return (torrentList);
    },

    /**
     * Return a list of "Torrent" instances (see Library/Torrent.class)
     * @param nbrTorrents
     * @returns {Promise<[]>}
     */
    getPopular : async function(nbrTorrents = this.nbrTorrentsPerSources) {
        let torrentList = [];
        let tmpTorrentList = [];

        let resultList = await Promise.all([
            this._getPopularFromThePirateBay(nbrTorrents),
            this._searchFromExtraTorrent('1080', nbrTorrents),
            this._searchFromRarbg('1080', nbrTorrents),
            this._searchFrom1337x('1080', nbrTorrents),
            this._getPopularFromEztu(nbrTorrents)
        ]).catch((err) => {
            //console.log(err)
        });

        /**
         * Concatenation of each provider's arrays in an global array
         */
        for (const result of resultList) {
            tmpTorrentList = tmpTorrentList.concat(result);
        }

        tmpTorrentList = tmpTorrentList.filter(torrent => {
            return (torrent !== undefined);
        });

        for (const torrent of tmpTorrentList) {
            const information = ParseTorrentTitle.parse(torrent.title || torrent.name);

            const title = information.title;
            const year = information.year || null;
            const seed = torrent.seeders || torrent.seeds || 0;
            try {
                const torrentid = parseTorrent(torrent.magnetLink || torrent.magnet_url || torrent.magnet).infoHash;
                torrentList.push(new Video({title: title, seed: seed, torrentid: torrentid, year: year}));
            } catch (e) { }
        }
        torrentList = this._cleanTorrentList(torrentList);

        return (torrentList);
    },

    searchTorrents : async function(title, year, imdbid, season = undefined, limit = 30) {
        let torrentList = [];
        let tmpTorrentList = [];
        let query;

        if (season)
            query = title + ' ' + season;
        else
            query = title + ' ' + year;
            
        let resultList = await Promise.all([
            this._searchFromThePirateBay(query, limit),
            this._searchFromRarbg(query, limit),
            this._searchFrom1337x(query, limit),
            this._searchFromEztu(imdbid, season)
        ]).catch((err) => {
            //console.log(err)
        });
        
        /**
         * Concatenation of each provider's arrays in an global array
         */
        for (const result of resultList) {
            tmpTorrentList = tmpTorrentList.concat(result);
        }

        tmpTorrentList = tmpTorrentList.filter(torrent => {
            return (torrent !== undefined);
        });


        torrentList = tmpTorrentList.map(torrent => {
            let tmpTorrent = {};
            
            tmpTorrent.seeders = torrent.seeds || parseInt(torrent.seeders) || 0;
            tmpTorrent.magnet = torrent.magnetLink || torrent.magnet_url || torrent.magnet;
            tmpTorrent.title = torrent.title || torrent.name;
            tmpTorrent.size = torrent.size || this._formatBytes(torrent.size_bytes) || null;
            tmpTorrent.source = torrent.source;
            try {
                tmpTorrent.tid = parseTorrent(tmpTorrent.magnet).infoHash;
            } catch (e) {}

            return tmpTorrent;
        });

        
        torrentList = torrentList.filter((t) => {
            return t.seeders;
        })


        torrentList = this._mergeDuplicated(torrentList)

        torrentList.sort((a, b) => {
            return b.seeders - a.seeders;
        })

        return (torrentList.slice(0, limit));
    },
    
    _mergeDuplicated : function (torrentList)
    {
        let mergedTorrentList = [];

        torrentList.forEach((t) => {
            let found = torrentList.filter((e) => {
                return e.tid === t.tid;
            })
            if (found.length > 1 && !mergedTorrentList.map(e => e.tid).includes(t.tid))
                mergedTorrentList.push(this._mergeTorrents(found))
        })

        torrentList = torrentList.filter((t) => {
            return !mergedTorrentList.map(e => e.tid).includes(t.tid);
        })

        return torrentList.concat(mergedTorrentList);
    },

    _mergeTorrents : function (dup)
    {
        let announces = [];
        let seeders = 0;
        let tmpTorrent;
        let info;

        dup.forEach((arg) => {
            info = parseTorrent(arg.magnet);
            announces.concat(info.announce);
            if (arg.seeders > seeders)
                seeders = arg.seeders;
        })

        info.announce = announces
        let magnet = parseTorrent.toMagnetURI(info);

        tmpTorrent = dup[0];
        tmpTorrent.seeders = seeders;
        tmpTorrent.magnet = magnet;

        return tmpTorrent;
    },

    _cleanTorrentList : function(torrentList) {
        let cleanedList = [];

        torrentList = torrentList.filter(object => {
            return (object !== undefined);
        });

        /**
         * Delete duplicate torrents
         */
        for (const element of torrentList) {
            let result = torrentList.filter(object => {
                return object.torrentid === element.torrentid
            });
            if (result.length > 1) {
                let torrentUnique = element.clone();
                for (const torrent of result) {
                    if (torrent.seed > torrentUnique.seed)
                        torrentUnique.seed = torrent.seed;
                    torrentList.splice(torrentList.indexOf(torrent), 1)
                }
                cleanedList = cleanedList.concat(result[0]);
            }
            else
                cleanedList = cleanedList.concat(result);
        }

        return (cleanedList);
    }
};

module.exports = TorrentCollector;