const fs = require('fs')
const config = JSON.parse(fs.readFileSync('./config/torrent-config.json'))

process.env.FFMPEG_BIN_PATH = __dirname + '/../' + config.ffmpeg.path
process.env.FFMPEG_PATH = __dirname + '/../' + config.ffmpeg.path

const Torrent = require('../Schemas/torrent.js');
const path = require('path')
const torrentStream = require('torrent-stream');
const ffmpeg = require('fluent-ffmpeg')
const TimeFormat = require('hh-mm-ss')
const { getVideoDurationInSeconds } = require('get-video-duration')
const http = require('http')
const cp = require('child_process');
const OS = require('opensubtitles-api');
const OpenSubtitles = new OS(config.OpenSubtitles);
const parseTorrent = require('parse-torrent')
const EventEmitter = require('events');
const cliProgress = require('cli-progress');

const multibar = new cliProgress.MultiBar({
  clearOnComplete: false,
  hideCursor: true,
  format: '{action} | [{bar}] {percentage}% | {filename} | {status} | {codec}'
}, cliProgress.Presets.shades_grey);

module.exports = class TorrentEngine extends EventEmitter {
  constructor(magnet, torrentPath = '') {
    super()
    this.files = [];
    this.sFiles = [];
    this.ready = false;
    this.engine;
    this.dbEntry
    this.tid;
    this.dlPath
    this.progressBars = []

    if (magnet != '' && torrentPath == '')
    {
      this.tid = parseTorrent(magnet).infoHash
      this.dlPath = config.downloadPath + this.tid + '/'
      try
      {
        fs.mkdirSync(config.downloadPath + this.tid)
      } catch {}
    }
    else if (magnet == '' && torrentPath != '')
    {
      file = fs.readFileSync(torrentPath)
      this.tid = parseTorrent(file).infoHash
      this.dlPath = config.downloadPath + this.tid + '/'
      try
      {
        fs.mkdirSync(config.downloadPath + this.tid)
      } catch {}
    }

    let that = this
    var getDbEntry = new Promise(function(resolve, reject){
    let query = {tid: that.tid}
    Torrent.findOne(query).then((torrent) => {
      //console.log(torrent)
      if (!torrent || torrent.length == 0)
      {
        //console.log('MONGOOSE NO RESULT')
        if (torrentPath != '')
          query = {path: torrentPath, tid: that.tid}
        else
          query = {magnet: magnet, tid: that.tid}
        Torrent.create(query, (err, torrent) => {
          //console.log(err)
          that.dbEntry = torrent._id;
          resolve()
        });
      }
      else
      {
        that.dbEntry = torrent._id;
        resolve()
      }
    })

  })

  getDbEntry.then(() => {
    if (torrentPath != '')
    {
      file = fs.readFileSync(torrentPath)
      this.engine = torrentStream(file, {path: this.dlPath});
    }
    else if (magnet != '')
      this.engine = torrentStream(magnet, {path: this.dlPath});
    //console.log('TORRENT ADDED')

    that.engine.on('ready', async () => {
      //console.log('ENGINE READY', that.engine)

      that.engine.exactPieceLength = that.engine.torrent.length / that.engine.torrent.pieces.length
      //that.engine.exactPieceLength = that.engine.torrent.pieceLength
      //that.engine.torrent.lastPieceLength
      that.engine.files.forEach(function (file, index) {
        file.deselect();
        file.index = index;
        file.ext = path.extname(file.path)
        Torrent.updateOne({_id: that.dbEntry, 'files.name': file.name},
        {$set: {'files.$.ext': file.ext }}, (e,r) => {
          //console.log(e,r)
        })
        if (config.vExt.includes(path.extname(file.path))) { //if file is video
           Torrent.findOne({ _id: that.dbEntry, 'files.name': file.name }, { _id: 0, files: { $elemMatch: { name: file.name } } },  (err, res) => {
            if (!res || (res.files[0].status != 'downloaded' && res.files[0].status != 'converted')) { //Not fully downlaoded
              //console.log(file.length, file.offset, file.path);
              Torrent.updateOne({ _id: that.dbEntry, 'files.name': { $ne: file.name } }, { $push: { files: { name: file.name, status: 'idle', path: file.path, pieceDownloaded: 0 } } }, (e, r) => {
                //console.log(e,r.nModified)
              });
              if (res && res.files[0].pieceDownloaded)
                file.pieceDownloaded = res.files[0].pieceDownloaded;
              else
                file.pieceDownloaded = 0;

              file.convStarted = false;
              file.subs = [];
              that.seekSubtitles(file);
              //file.hasLastPiece = file.offset + file.length === that.engine.torrent.length; //Last piece in this file
              //file.pieceNumber = Math.floor(file.length / that.engine.exactPieceLength) - 1 + file.hasLastPiece;

              file.pieceNumber = Math.round(file.length / that.engine.exactPieceLength);

              if (res)
                file.vCodec = res.files[0].vCodec
              that.files[path.basename(file.name, path.extname(file.name))] = (file);
              nextIndex()
            }
            else { //Fully downloaded
              //console.log(file.name, 'ALREADY DOWNLOADED');
              file = res.files[0].toObject();
              //console.log(file)
              file.index = index;
              //file.ext = path.extname(that.engine.files[index].path)
              that.engine.files[index] = file;
              that.files[path.basename(file.name, path.extname(file.name))] = (file);
              file.dlPct = 100;
              file.lastDownloadedPiecePct = 100;
              if (!that.progressBars[file.name])
                that.progressBars[file.name] = multibar.create(100, 0, {filename: file.name, action:'DOWNLOAD', status: 'DOWNLOADED', codec: file.vCodec});
              that.progressBars[file.name].update(file.dlPct, {codec: file.vCodec});
              nextIndex()
            }
          });
        }
        else if (config.sExt.includes(path.extname(file.path))) { //Subtitles files
          //console.log(file.length, file.offset, file.path);
          //file.createReadStream()
          that.sFiles[path.basename(file.name, path.extname(file.name))] = (file);
          //sFiles.push(file)
          nextIndex()
        }
        else
          nextIndex()

      });
  });

  let indexForEach = 0
  function nextIndex() {
    indexForEach++
    if (indexForEach == that.engine.files.length)
    {
      //console.log('TORRENT READY', indexForEach, that.engine.files.length, that.files)
      that.ready = true;
      that.emit('torrentReady', that.getList());
    }
  }

  that.engine.on('download', (e) => {
        Object.keys(that.files).forEach((k) => {
            let file = that.files[k];

            let fPiece = (file.offset / that.engine.torrent.pieceLength)
            let lPiece = ((file.offset + file.length - 1) / that.engine.torrent.pieceLength)

            if (fPiece <= e && e <= lPiece) //This piece is in FILE
            {
              file.pieceDownloaded += 1;
              file.lastDownloadedPiecePct = (file.pieceDownloaded / file.pieceNumber) * 100;
              if (!file.duration)
              {
                getVideoDurationInSeconds(that.dlPath + file.path).then((duration) => {
                  ffmpeg.ffprobe(that.dlPath + file.path, function(err, metadata) { //Get codec
                    //console.log(err)
                    if (metadata)
                    {
                    metadata.streams.forEach(function(stream){
                      if (stream.codec_type === "video")
                      {
                          //console.log(stream)
                          file.vCodec = stream.codec_name;
                          Torrent.updateOne({_id: that.dbEntry, 'files.name': file.name},
                          {$set: {'files.$.vCodec': file.vCodec }}, (e,r) => {})
                        }
                      });
                      file.duration = duration;
                    }
                  });

                }, (rej) => {
                  if (file.lastDownloadedPiecePct > 1.5)
                    that.emit('streamError',  {source: file.source, master: file.master, subs: file.subs, id: file.index, tid: that.tid, filename: file.name, index: file.index, tid: that.tid}) //File maybe not streamable
                })
              }
              else (file.duration)
                file.downloadedDuration = file.duration * (file.lastDownloadedPiecePct / 100)
              that.emit('downloadProgress', {dlTime: file.downloadedDuration, dlPct: file.lastDownloadedPiecePct, filename: file.name, index: file.index, tid: that.tid});
              if (file.lastDownloadedPiecePct > 0 && !file.ffcmd && !file.convStarted)
              {
                if(file.ext != '.mp4' || (file.vCodec && file.vCodec != 'h264'))
                {
                  //console.log(file.ext, file.vCodec)
                  this._convert(file)
                }
                else
                {
                  //this._convert(file, 1)
                  file.source = that.dlPath + file.path
                }
              }
              if(file.ext == '.mp4' && file.vCodec == 'h264' && file.duration && file.downloadedDuration > 60)
                that.emit('readyToStream', {source: file.source, master: file.master, subs: file.subs, id: file.index, tid: that.tid, filename: file.name, index: file.index, tid: that.tid})
              Torrent.updateOne({_id: that.dbEntry, 'files.name': file.name},
              {$set: {'files.$.pieceDownloaded': file.pieceDownloaded }}, (e,r) => {
                //console.log(e,r)
              })
              if (file.pieceDownloaded >= file.pieceNumber - 2)
              {
                //console.log('downloaded')
                Torrent.updateOne({_id: that.dbEntry, 'files.name': file.name},
                {$set: {'files.$.status': 'downloaded', 'files.$.lastDownloadedPiecePct': file.lastDownloadedPiecePct}}, (e,r) => {
                  //console.log(e,r)
                })

              }
            //console.log('DOWNLOAD', file.name, file.lastDownloadedPiecePct)
            if (!that.progressBars[file.name])
              that.progressBars[file.name] = multibar.create(100, 0, {filename: file.name, action:'DOWNLOAD', status: 'DOWNLOADING', codec: file.vCodec});
            that.progressBars[file.name].update(file.lastDownloadedPiecePct, {status: `${file.pieceDownloaded}/${file.pieceNumber}`, codec: file.vCodec});

            //If download is 1 min ahead convert, resume convert
            if (file.convPaused && file.downloadedDuration > file.currentTime + 60)
            {
              file.ffcmd.kill('SIGCONT')
              file.convPaused = false
            }
          }
        }) //End video file

        //Subtites files
        Object.keys(that.sFiles).forEach((k) => {
          let file = that.sFiles[k];

          let fPiece = parseInt(file.offset / that.engine.exactPieceLength)
          let lPiece = parseInt((file.offset + file.length) / that.engine.exactPieceLength)

          if (fPiece <= e && e <= lPiece) //This piece is in FILE
          {
            file.lastDownloadedPiecePct = (e - fPiece) / (lPiece - fPiece) * 100
            //console.log('DOWNLOAD SRT', file.name, file.lastDownloadedPiecePct)
                cp.spawn('./bin/ffmpeg', [
                  '-y',
                  '-i "' + that.dlPath + file.path + '"',
                  '-f webvtt',
                  '"' + that.dlPath + file.path + '.vtt"'
              ],
              { shell: true }).on('close', (code) => {
                  //console.log(`child process exited with code ${code}`);
                  if (code == 0) //SRT converted to VTT
                  {
                    if (this.files[path.basename(file.name, path.extname(file.name))])
                    {
                      this.files[path.basename(file.name, path.extname(file.name))].subs.push({path: that.dlPath + file.path + '.vtt', langcode: '', lang: 'Unknown'}) //Add to subs
                      this.emit('subsAdded', {tid: this.tid, id: this.files[path.basename(file.name, path.extname(file.name))].index,path: that.dlPath + file.path + '.vtt', langcode: '', lang: 'Unknown'})
                      Torrent.updateOne({_id: that.dbEntry, 'files.name': file.name, 'files.subs.path' : {$ne: that.dlPath + file.path + '.vtt'}},
                        {$push : {'files.$.subs' : {path: that.dlPath + file.path + '.vtt', langcode: '', lang: 'Unknown'}}}, (e,r) => {
                      })
                    }
                    try {
                      fs.unlink(that.dlPath + file.path, () => { }) //Delete original SRT
                    } catch {}
                  }
                });

          }
        }) //End subfiles
      }) // End downlaod event
    }) //End DB promise
  } //End constructor

  selectFile(index, callback)
  {
    //this.pauseAll();

    let that = this
    let file = this.engine.files[index]
    //console.log(file)
    //console.log(file.ext)
    if (!that.progressBars[file.name])
      this.progressBars[file.name] = multibar.create(100, 0, {filename: file.name, action:'DOWNLOAD', status: 'SELECTED', codec: file.vCodec});
    if (this.sFiles[path.basename(file.name, path.extname(file.name))])
      this.sFiles[path.basename(file.name, path.extname(file.name))].createReadStream();
    if (!file.lastDownloadedPiecePct)
       file.lastDownloadedPiecePct = (file.pieceDownloaded / file.pieceNumber) * 100
    this.emit('downloadStarted', {filename: file.name, index: file.index, tid: that.tid})
    if (file.lastDownloadedPiecePct < 99.8 && !file.downloading) //File not fully downloaded and not downloading
    {
      file.downloading = true;
      file.createReadStream()

      //this.emit('downloadStarted', {filename: file.name, index: file.index, tid: that.tid})
      //callback({code:3, msg:'Download Started', dlPct: file.lastDownloadedPiecePct, convPct: file.convPct})
      Torrent.updateOne({_id: that.dbEntry, 'files.name': file.name}, {$set: {'files.$.status': 'downloading'}})
    } else //File fully downloaded or downloading
    {
      that.emit('downloadProgress', {dlTime: file.downloadedDuration, dlPct: file.lastDownloadedPiecePct, filename: file.name, index: file.index, tid: that.tid});
      if (file.convStarted && file.convPct > 0.2) //If converting
      {
        callback({code: 0, msg: 'Ready to stream', dlPct: file.lastDownloadedPiecePct, convPct: file.convPct, thumbnail: file.thumbPath})
        that.emit('readyToStream', {master: file.master, subs: file.subs, id: file.index, tid: that.tid, filename: file.name, index: file.index, tid: that.tid})
      }
      else if (file.status == 'converted') //If converted
      {
        that.emit('readyToStream', {master: file.master, subs: file.subs, id: file.index, tid: that.tid, filename: file.name, index: file.index, tid: that.tid})
        callback({code: 0, msg: 'Ready to stream', dlPct: file.lastDownloadedPiecePct, convPct: file.convPct, thumbnail: file.thumbPath, filename: file.name, index: file.index})
      }
      else //Not converting and not converted
      {
        if(file.ext != '.mp4' || (file.vCodec && file.vCodec != 'h264'))
        {
          //console.log(file.ext, file.vCodec)
          this._convert(file)
          callback({code: 1, msg: 'File not ready to stream', dlPct: file.lastDownloadedPiecePct, convPct: file.convPct})
        }
        else
        {
          //this._convert(file, 1)
          //callback({code: 1, msg: 'File not ready to stream', dlPct: file.lastDownloadedPiecePct, convPct: file.convPct})
          file.source = that.dlPath + file.path;
          that.emit('readyToStream', {source: that.dlPath + file.path, master: file.master, subs: file.subs, id: file.index, tid: that.tid, filename: file.name, index: file.index, tid: that.tid})
          callback({code: 0, msg: 'Ready to stream', dlPct: file.lastDownloadedPiecePct, convPct: file.convPct, thumbnail: file.thumbPath, filename: file.name, index: file.index})
        }
      }
    }
  }

  _getIncludedSubtitles(file)
  {
    let that =this
    if (!file.subtitlesGetted)
    {
      file.subtitlesGetted = true;
      let probe = cp.spawn('./bin/ffprobe', [
          '"' + that.dlPath + file.path + '"',
          '-loglevel error',
          '-select_streams s',
          '-show_entries stream=index:stream_tags=language',
          '-of csv=p=0'
      ],
      { shell: true })

      probe.stderr.on('data', (data) => {
          //console.error(`stderr: ${data}`);
        });

      probe.stdout.on('data', (data) => {
          let subs = data.toString().split('\n')
          //console.log(subs)
          subs.forEach((sub) => {
              sub = sub.split(',')
              cp.spawn('./bin/ffmpeg', [
                  '-y',
                  '-i "' + that.dlPath + file.path + '"',
                  '-map 0:' + sub[0],
                  '-f webvtt',
                  '"' + that.dlPath + file.path + sub.join('') + '.vtt"'
              ],
              { shell: true }).on('close', (code) => {
                  //console.log(`child process exited with code ${code}`);
                  if (code == 0)
                  {
                      file.subs.push({path: that.dlPath + file.path + sub.join('') + '.vtt', langcode: sub[1], lang: sub.join('')})
                      that.emit('subsAdded', {tid: that.tid, id: file.index, path: that.dlPath + file.path + sub.join('') + '.vtt', langcode: sub[1], lang: sub.join('')})
                      Torrent.updateOne({_id: that.dbEntry, 'files.name': file.name, 'files.subs.path' : {$ne: that.dlPath +  file.path + sub.join('') + '.vtt'}},
                        {$push : {'files.$.subs' : {path: that.dlPath +  file.path + sub.join('') + '.vtt', langcode: sub[1], lang: sub.join('')}}}, (e,r) => {})
                  }
              });
          })
      });
    }
  }

  seekSubtitles(file)
  {
    let that = this
    if (!file.OSsubtitlesGetted)
    {
      file.OSsubtitlesGetted = true;
      OpenSubtitles.search({
        size: file.length,
        query: path.basename(file.path),        // Complete path to the video file, it allows
        }).then(id => {
      for (let [n, sub] of Object.entries(id)) {
        if (['fr', 'en', 'es', 'de'].includes(n))
        {
          Torrent.updateOne({_id: that.dbEntry, 'files.name': file.name, 'files.subs.path' : {$ne: that.dlPath + file.path + sub.langcode + ".vtt"}},
            {$push : {'files.$.subs' : {langcode: sub.langcode, lang: sub.lang, path: that.dlPath +  file.path + sub.langcode + ".vtt"}}}, (e,r) => {
            })
          file.subs.push({langcode: sub.langcode, lang: sub.lang, path: that.dlPath + file.path + sub.langcode + ".vtt"})
          that.emit('subsAdded', {tid: that.tid, id:file.index, langcode: sub.langcode, lang: sub.lang, path: that.dlPath + file.path + sub.langcode + ".vtt"})
          try {
            const subfile = fs.createWriteStream(that.dlPath + file.path + sub.langcode + ".vtt");
            http.get(sub.vtt, function(response) {
              response.pipe(subfile);
            });
          } catch(e) {}
      }
      }
    })
  }
  }

  _convert(file, isMp4 = 0)
  {
    file.convStarted = true;

    let that = this
    getVideoDurationInSeconds(that.dlPath + file.path).then((duration) => {

      file.duration = duration
      file.downloadedDuration = duration * (file.lastDownloadedPiecePct / 100)
      if (duration * file.lastDownloadedPiecePct < 120)
        return file.convStarted = false;
      if (!this.progressBars[file.name+'CONV'])
        this.progressBars[file.name+'CONV'] = multibar.create(100, 0, {filename: file.name, action:'CONVERT'});

      file.vTime = duration

      var opts = [
        '-start_number 0',
        '-hls_time 10',
        '-hls_list_size 0',
        '-f hls',
        '-hls_flags single_file' //optional, cleaner
      ]

      if (isMp4)
      {
        opts = [
          '-c copy'
        ].concat(opts)
      }
      else{
      opts = [
        '-preset ultrafast',
        //'-map 0:a', '-map 0:v', //Fail on some files
        '-threads 2',
        '-crf 23',
        '-maxrate 2.4M',
        //'-vf scale=-1:360',
        '-b:a 128k',
        '-c:v libx264',
        '-x264opts keyint=60:no-scenecut',
        '-c:a libmp3lame'
      ].concat(opts)
    }
    //console.log(opts)

      file.ffcmd = ffmpeg(that.dlPath + file.path).addOptions(opts)
        .on('error', (e, stdout, stderr) => {
          this._conversionFailed(file, stderr)
        })
        .on('progress', (e) => {
          file.currentTime = TimeFormat.toS(e.timemark)
          //console.log('CONV: ', file.name, (file.currentTime / file.vTime) * 100)
          file.convPct = (file.currentTime / file.vTime) * 100
          that.emit('convProgress', {convPct: file.convPct, filename: file.name, index: file.index, tid: that.tid, convTime: file.currentTime})
          this.progressBars[file.name+'CONV'].update(file.convPct, {status: `${file.currentTime}/${file.vTime}`})

          if (file.convStarted && file.currentTime > 30)
          {
              //file.readyToStream = true
              that.emit('readyToStream', {master: file.master, source: file.source, subs: file.subs, id: file.index, tid: that.tid, filename: file.name, index: file.index, tid: that.tid})
          }

          //If dowmload is less than 1 min ahead convert, pause convert
          if (file.vTime - file.currentTime > 180 && file.downloadedDuration < file.currentTime + 60)
          {
            file.convPaused = true;
            file.ffcmd.kill('SIGSTOP')
            that.progressBars[file.name+'CONV'].update(file.convPct, {status: 'PAUSED'})
          }
      })
      .on('start', function(commandLine) {
        {
          file.ffmpegCalled = true
          //console.log('Spawned Ffmpeg with command: ' + commandLine);
          that.progressBars[file.name+'CONV'].update(file.convPct, {status: 'SPAWN FFMPEG'})
          that.emit('convStarted', {dlPct: file.lastDownloadedPiecePct, convPct: file.convPct, filename: file.name, index: file.index, tid: that.tid})
        }
      }).on('end', (stdout, stderr) => {
        //console.log('END CONV stdout:', stdout, 'stderr:', stderr)
        that.progressBars[file.name+'CONV'].update(file.convPct, {status: 'END CONV'})

        if (stderr == '' ||  stdout != '')
        {
          that.progressBars[file.name+'CONV'].update(file.convPct, {status: 'CONV OK'})
          try {
            fs.unlink(that.dlPath + file.path, () => {}) //Delete original
          } catch(e) {}
          Torrent.updateOne({_id: that.dbEntry, 'files.name': file.name},
          {'files.$.status': 'converted', 'files.$.lastStatus': Date.now()}, (e,r) => {
            //console.log('END UPDATE',file.name, e,r)
          })
        }
        else
          this._conversionFailed(file, stderr)
      }).output(that.dlPath + file.path + '.m3u8')

        file.master = that.dlPath + file.path + '.m3u8'

        Torrent.updateOne({_id: that.dbEntry, 'files.name': file.name},
        {$set: {'files.$.master': file.master}}, (e,r) => {})

        this._getIncludedSubtitles(file)
        if (!file.ffmpegCalled) //Delete previous files before restart
        {
          try{
          fs.unlink(that.dlPath + file.path + '.m3u8', () => {
            file.ffcmd.run()
          })} catch(e) {
            this._conversionFailed(file, e, 0)
          }
        }
    }, (reject) =>
    {
      this._conversionFailed(file, reject, 0)
      //console.log('Cannot get duration')
      if (file.lastDownloadedPiecePct > 1.5)
        that.emit('streamError', {source: file.source, master: file.master, subs: file.subs, id: file.index, tid: that.tid, filename: file.name, index: file.index, tid: that.tid}) //File maybe not streamable
    })
  }

  _conversionFailed(file, err = '', event = 1)
  {
    //console.log('Conversion FAILED', err)
    if (this.progressBars[file.name+'CONV'])
      this.progressBars[file.name+'CONV'].update(file.convPct, {status: 'FAILED'})
    file.convStarted = false;
    file.ffcmd = undefined;
    file.convPct = undefined
    file.ffmpegCalled = false
    if (event)
      this.emit('convFailed', {filename: file.name, index: file.index, tid: this.tid})
    if (file.lastDownloadedPiecePct >= 99.8) //Auto restart if file was fully downloaded
      this._convert(file)
  }

  getList()
  {
    if (!this.ready)
      return({code: 1, msg:'Torrent not ready'})
    return (Object.keys(this.files).map((key, index) => {
      return {id: this.files[key].index, name: key}
    }));
  }

  getFileInfo(id)
  {
    let file = this.engine.files[id]
    return {subs: file.subs, hls: file.master, source: file.source, name: file.name}
  }

  startFile(id, callback)
  {
    this.selectFile(id, (r) => {
        callback(r)
    })
  }

}
