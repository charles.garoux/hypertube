const Torrent = require('../Schemas/torrent.js');
const path = require('path');
let fs = require('fs')
const config = JSON.parse(fs.readFileSync('./config/torrent-config.json'))

module.exports = class TorrentManager
{
    static deleteOrphanTorrents()
    {
        let dirs = [];
        try {
           dirs = fs.readdirSync(`${__dirname}/../${config.downloadPath}`);
        } catch (err) {}
        dirs.forEach((dir) => {
            Torrent.find({tid: dir}, {}, (err, res) => {
            if (!res || res.length === 0)
            {
                try {
                    fs.rmdirSync(`${__dirname}/../${config.downloadPath}${dir}`, {recursive: true})
                } catch (err) {}
            }
            })
        })
    }

    static deleteOrphanEntries()
    {
        Torrent.find({}, {}, (err, res) => {
            res.forEach((torrent) => {
                if (!fs.existsSync(`${__dirname}/../${config.downloadPath}${torrent.tid}`)) //Folder doesn't exits
                    Torrent.deleteMany({tid: torrent.tid}, (err, res) => { })
            })
        })
    }

    static deleteOldTorrents()
    {
        var oneMonth = new Date();
        oneMonth.setMonth(oneMonth.getMonth() - 1);
        Torrent.find({'files.lastStatus': {$lte: oneMonth}}, {tid: 1, 'files.$': 1}, (err, res) => {
            res.forEach((torrent) => {
            torrent.files.forEach((file) => {
                let regex = RegExp(`${path.basename(file.path.slice(0, -4))}`.replace(/[-[\]{}()*+?.,\\^$|]/g, "\\$&"), 'g')
                files = fs.readdirSync(`${__dirname}/../${config.downloadPath}${torrent.tid}/${path.dirname(file.path)}`)
                .filter(p => regex.test(p))
                .map(p => fs.unlinkSync(`${__dirname}/../${config.downloadPath}${torrent.tid}/${path.dirname(file.path)}/${p}`))
                Torrent.updateMany({tid: torrent.tid}, {$pull: {files: {_id: file._id}}}, (err, res) => {})
            })
            })
        })
    }
}