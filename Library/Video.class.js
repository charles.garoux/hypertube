class Video {
    imdbid = null;
    score = 0;
    genreList = [];
    torrentid = null;
    title = null;
    seed = null;
    poster = null;
    viewed = null;
    year = null;

    constructor(SourceObject) {
        for (const prop in this) {
            if (SourceObject[prop])
                this[prop] = SourceObject[prop];
        }
    };

    /**
     * Parse imdb data from API and put them in the instance
     * @param imdbData
     */
    addImdbData(imdbData) {
        this.imdbid = imdbData.imdbid;
        this.title = imdbData.title;
        this.poster = imdbData.poster;
        this.year = imdbData.year || imdbData.start_year;
        this.genreList = imdbData.genres.split(', ');
        this.score = imdbData.rating;
    };

    clone () {
      return (new Video(this));
    };
}

module.exports = Video;