
let AuthMiddleware = {
    check : function (req, res, next) {
        if (!req.user) {
            res.redirect('/login');
        } else {
            next();
        }
    },

    notCheck : function (req, res, next) {
        if (!req.user) {
            next();
        } else {
            res.redirect('/');
        }
    }
};

module.exports = AuthMiddleware;