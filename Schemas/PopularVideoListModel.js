const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PopularVideoListSchema = new Schema({
    popularVideoList: {type: [{
            imdbid: {type: String, default: null},
            score: {type: Number, default: null},
            genreList: {type: Array, default: null},
            torrentid: {type: String, default: null},
            title: {type: String, default: null},
            poster: {type: String, default: null},
            seed: {type: Number, default: null},
            year: {type: Number, default: null},
        }], default: null},
    timestampOfCreation: {type: Number, default: Date.now()},
});

const PopularVideoListModel = mongoose.model('popularVideoList', PopularVideoListSchema);

module.exports = PopularVideoListModel;