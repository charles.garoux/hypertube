const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectID = require('mongodb').ObjectID

const commentSchema = new Schema({
    imdbid: String,
    uid: ObjectID,
    text: String,
    date: Date
});

const Comment = mongoose.model('comment', commentSchema);

module.exports = Comment;
