const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const torrentSchema = new Schema({
    magnet: String,
    path: String,
    tid: String,
    dlPath: String,
    files: [
        {name: String,
        path: String,
        master: String,
        status: String,
        lastStatus: { type: Date, default: Date.now },
        thumbnail: String,
        pieceDownloaded: Number,
        vCodec: String,
        ext: String,
        subs: [
            {path: String,
            langcode: String,
            lang: String}
        ]}
    ]
});

const Torrent = mongoose.model('torrent', torrentSchema);

module.exports = Torrent;
