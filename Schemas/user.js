const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  googleId: String,
  fortyTwoId: String,
  twitterId: String,
  githubId: String,
  mail: String,
  pass: String,
  picture: String,
  username: String,
  firstname: String,
  lastname: String,
  lang: String,
  access: Boolean,
  token: String,
  comments: [{
    imdb: String,
    text: String,
    date: Date
  }],
  views: [{
    imdb: String,
    title: String,
    date: Date
  }]
});

const User = mongoose.model('user', userSchema);

module.exports = User;
