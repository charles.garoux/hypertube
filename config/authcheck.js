const authCheck = (req, res, next) => {
    if (!req.user) {
        res.redirect('/login');
    } else {
        next();
    }
};

const notAuthCheck = (req, res, next) => {
    if (!req.user) {
        next();
    } else {
        res.redirect('/');
    }
};

module.exports = {authCheck, notAuthCheck};