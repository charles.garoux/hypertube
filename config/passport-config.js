const colors = require('colors/safe');
const passport = require('passport');
const passwordHash = require('password-hash');
const GoogleStrategy = require('passport-google-oauth20');
const FortyTwoStrategy = require('passport-42').Strategy;
const TwitterStrategy = require('passport-twitter').Strategy;
const GitHubStrategy = require('passport-github').Strategy;
const User = require('../Schemas/user.js');

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id).then((user) => {
    done(null, user);
  });
});

passport.use(

  new GitHubStrategy({
    clientID: 'b6f876f95b20f0920267',
    clientSecret: '23423f1c2962c5234fdd40873b62c03533eb3b3b',
    callbackURL: "/auth/github/redirect"
  }, (accessToken, refreshToken, profile, done) => {

    User.findOne({githubId: profile.id}).then((currentUser) => {

      if (currentUser) {

        console.log("User " + colors.yellow(currentUser.username) + " is now connected");
        done(null, currentUser);

      }
      else {

        User.findOne({username: profile.username.toLowerCase()}).then((user) => {

          if (user) {

            User.find({}, function(err, users) {
              var userMap = [];
              var i = 0;

              users.forEach(function(user) {
                userMap[i] = user;
                i++;
              });


              while(1) {

                var j = 0;
                var tabL = userMap.length;
                var username = profile.username.toLowerCase() + (Math.floor(Math.random() * (100000 - 1000 +1)) + 1000);
                var count = 0;

                while (j < tabL) {
                  if (username === userMap[j].username){
                    count++;
                    break;
                  }
                  j++;
                }

                if (count === 0) {

                  new User({
                    githubId: profile.id,
                    picture: profile.photos[0].value,
                    username: username,
                    firstname: '',
                    lastname: '',
                    lang: "en"
                  }).save().then((newUser) => {
                    console.log('New user created: ' + colors.yellow(newUser.username) + ' with github oauth');
                    done(null, newUser);
                  });

                  break;
                }

              }

            });

          }
          else {

            new User({
              githubId: profile.id,
              picture: profile.photos[0].value,
              username: profile.username,
              firstname: '',
              lastname: '',
              lang: "en"
            }).save().then((newUser) => {
              console.log('New user created: ' + colors.yellow(newUser.username) + ' with github oauth');
              done(null, newUser);
            });

          }

        });

      }

    });
  })

);

passport.use(
  new GoogleStrategy({
    callbackURL: "/auth/google/redirect",
    clientID: "446027399430-pnm6u1u5k03voi885eqi06nj8p56b4ki.apps.googleusercontent.com",
    clientSecret: "6BFTs8YpGjWHP1FKtNbrUmwU"
  }, (accessToken, refreshToken, profile, done) => {

    User.findOne({googleId: profile.id}).then((currentUser) => {

      if (currentUser) {

        console.log("User " + colors.yellow(currentUser.username) + " is now connected");
        done(null, currentUser);

      }
      else {

        User.findOne({username: profile.displayName.toLowerCase()}).then((user) => {

          if (user) {

            User.find({}, function(err, users) {
              var userMap = [];
              var i = 0;

              users.forEach(function(user) {
                userMap[i] = user;
                i++;
              });


              while(1) {

                var j = 0;
                var tabL = userMap.length;
                var username = profile.displayName.toLowerCase() + (Math.floor(Math.random() * (100000 - 1000 +1)) + 1000);
                var count = 0;

                while (j < tabL) {
                  if (username === userMap[j].username){
                    count++;
                    break;
                  }
                  j++;
                }

                if (count === 0) {

                  new User({
                    googleId: profile.id,
                    picture: profile.photos[0].value,
                    username: username,
                    firstname: profile.name.givenName,
                    lastname: profile.name.familyName,
                    lang: "en"
                  }).save().then((newUser) => {
                    console.log('New user created: ' + colors.yellow(newUser.username) + ' with google oauth');
                    done(null, newUser);
                  });

                  break;
                }

              }

            });

          }
          else {

            new User({
              googleId: profile.id,
              picture: profile.photos[0].value,
              username: profile.displayName.toLowerCase(),
              firstname: profile.name.givenName,
              lastname: profile.name.familyName,
              lang: "en"
            }).save().then((newUser) => {
              console.log('New user created: ' + colors.yellow(newUser.username) + ' with google oauth');
              done(null, newUser);
            });

          }

        });

      }

    });

  })
);

passport.use(
  new FortyTwoStrategy({
    callbackURL: "/auth/42/redirect",
    clientID: "6dbd44efba50fdedadccbf58583bcd2b839e7da93aaf5d6348bde80bf31c9e06",
    clientSecret: "51c29ca3b0aa8f9a1b34d68bda29b466b1a77005ea0bc7bef9951050d529b720"
  }, (accessToken, refreshToken, profile, done) => {

    User.findOne({fortyTwoId: profile.id}).then((currentUser) => {

      if (currentUser) {

        console.log("User " + colors.yellow(currentUser.username) + " is now connected");
        done(null, currentUser);

      }
      else {

        User.findOne({username: profile.username.toLowerCase()}).then((user) => {

          if (user) {

            User.find({}, function(err, users) {
              var userMap = [];
              var i = 0;

              users.forEach(function(user) {
                userMap[i] = user;
                i++;
              });


              while(1) {

                var j = 0;
                var tabL = userMap.length;
                var username = profile.username.toLowerCase() + (Math.floor(Math.random() * (100000 - 1000 +1)) + 1000);
                var count = 0;

                while (j < tabL) {
                  if (username === userMap[j].username){
                    count++;
                    break;
                  }
                  j++;
                }

                if (count === 0) {

                  new User({
                    fortyTwoId: profile.id,
                    picture: profile.photos[0].value,
                    username: username,
                    firstname: profile.name.givenName,
                    lastname: profile.name.familyName,
                    lang: "en"
                  }).save().then((newUser) => {
                    console.log('New user created: ' + colors.yellow(newUser.username) + ' with 42 oauth');
                    done(null, newUser);
                  });

                  break;
                }

              }

            });

          }
          else {

            new User({
              fortyTwoId: profile.id,
              picture: profile.photos[0].value,
              username: profile.username.toLowerCase(),
              firstname: profile.name.givenName,
              lastname: profile.name.familyName,
              lang: "en"
            }).save().then((newUser) => {
              console.log('New user created: ' + colors.yellow(newUser.username) + ' with 42 oauth');
              done(null, newUser);
            });

          }

        });

      }

    });

  })
);

passport.use(
  new TwitterStrategy({
    callbackURL: "/auth/twitter/redirect",
    consumerKey: "ewmz06bUVmxZDGR738PyceITG",
    consumerSecret: "CRTVWKEfAdvTHOWlJ0JnS9RvJNXHX50r7gn8NAXJ7JU2paAaXy"
  }, (accessToken, refreshToken, profile, done) => {

    User.findOne({twitterId: profile.id}).then((currentUser) => {

      if (currentUser) {

        console.log("User " + colors.yellow(currentUser.username) + " is now connected");
        done(null, currentUser);

      }
      else {

        User.findOne({username: profile.username.toLowerCase()}).then((user) => {

          if (user) {

            User.find({}, function(err, users) {
              var userMap = [];
              var i = 0;

              users.forEach(function(user) {
                userMap[i] = user;
                i++;
              });


              while(1) {

                var j = 0;
                var tabL = userMap.length;
                var username = profile.username.toLowerCase() + (Math.floor(Math.random() * (100000 - 1000 +1)) + 1000);
                var count = 0;

                while (j < tabL) {
                  if (username === userMap[j].username){
                    count++;
                    break;
                  }
                  j++;
                }

                if (count === 0) {

                  new User({
                    twitterId: profile.id,
                    picture: profile.photos[0].value,
                    username: username,
                    lang: "en"
                  }).save().then((newUser) => {
                    console.log('New user created: ' + colors.yellow(newUser.username) + ' with twitter oauth');
                    done(null, newUser);
                  });

                  break;
                }

              }

            });

          }
          else {

            new User({
              twitterId: profile.id,
              picture: profile.photos[0].value,
              username: profile.username.toLowerCase(),
              lang: "en"
            }).save().then((newUser) => {
              console.log('New user created: ' + colors.yellow(newUser.username) + ' with twitter oauth');
              done(null, newUser);
            });

          }

        });

      }

    });

  })
);
