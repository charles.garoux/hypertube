const niv = require('node-input-validator');

niv.extend('imdbid', ({ value }) => {
  var regex = /tt\d{7,8}$/;
  return(regex.test(value));

});

niv.extend('magnet', ({ value }) => {
  let match = value.match('magnet:\?xt=urn:btih:[a-z]|[A-Z]|[0-9]*.*')
  return match && match[0] === value;
});

niv.extend('password', ({ value }) => {
  var regPass = /^(?=.*[\d])(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&_*])[\w!@#$%^&_*]{6,}$/;
  return(regPass.test(value));
});

niv.extend('lang', ({ value }) => {
  if (value === "en" || value === "fr" || value === "de" || value === "es") {
    return true;
  } else {
    return false;
  }
});

niv.extend('pictureFormat', ({ value }) => {
  var formatCheck = value.split(',');
  if (formatCheck[0] == "data:image/png;base64" || formatCheck[0] == "data:image/jpeg;base64") {
    return true;
  }
  else {
    return false;
  }
});

niv.extend('pictureSize', ({ value }) => {
  var sizeCheck = value.split(',');
  var size = Math.round(sizeCheck[1].length * 3/4);
  if (size > 0 && size <= 2000000) {
    return true;
  }
  else {
    return false;
  }
});

niv.extendMessages({
  'password': 'Password must contain at least one number, one uppercase letter, one lowercase letter, and one special character.',
  'lang': 'Bad language.',
  'pictureFormat': 'Wrong image format, accept formats are png and jpeg.',
  'pictureSize': 'Wrong image size, max size are 2 Mo.'
}, 'en');

niv.addCustomMessages({
  'pass.minLength': 'The password can not be less than 5.',
  'cpass.minLength': 'The confirm password can not be less than 5.',
  'pass.maxLength': 'The password can not be greater than 50.',
  'cpass.maxLength': 'The confirm password can not be greater than 5.'
});
