
$(document).ready(function(){

  var imdbid = $("#addComment #imdbid")[0].value;

  $.post('/comments/select/' + imdbid).done((res) => {
    if (res.data) {
      $( ".video-comment" ).html('');
      if (res.data.length > 0) {
        var i = 0;
        while (i < res.data.length) {
          displayComment(res.data[i])
          i++;
        }
      }
      else {
        $( ".video-comment" ).append(
          "<p class='no-comment'>No comment</p>"
        );
      }
    }
  });

});

function addComment() {

  var text = $("#addComment #comment")[0].value;
  var imdbid = $("#addComment #imdbid")[0].value;

  if (text !== '' && text.length > 2 && text.length <= 200) {
    $.post('/comments/add/' + imdbid, {comment: text}).done((res) => {
        if (res.err) {
          $( ".error" ).html(res.err);
        }
        else {
          $( ".error" ).html('');
          if (typeof $( ".no-comment" ) != 'undefined') {
            $( ".no-comment" ).html('');
          }
          $("#addComment #comment")[0].value = '';
          displayComment(res.data, 1)
        }
    });
  }
  else {
    $( ".error" ).html("Error a comment must be between 3 and 200 characters");
  }

};

function displayComment(data, before = 0)
{
  var temp = data.date.split('T')
  var day = temp[0].replace(/-/g, '/');
  var min = temp[1].split(':');
  var html = "<div class='comment'>"

  if (data.picture) 
    html +=  "<img class='comment-image' src='" + data.picture + "'/>";
  else 
    html += "<i class='no-comment-image far fa-user-circle'></i>";

  html += "<div class='comment-text'>" +
  "<p class='comment-username'><a href='../profile/" + data.username + "'>" + data.username + "</a> <span class='comment-date'> the " + day + " at " + min[0] + ":" + min[1] + "</span></p>" +
  "<p class='comment-comment'>" + data.text + "</p>" +
  "</div>" +
  "</div>";

  if (before)
    $( ".video-comment" ).prepend(html);
  else
    $( ".video-comment" ).append(html);
}
