
function rangeReviewMin() {

  var min = document.getElementById('rangeReviewMin');
  var max = document.getElementById('rangeReviewMax');
  var m = document.getElementById('reviewMin');

  if (parseInt(min.value, 10) >= parseInt(max.value, 10)) {
    min.value = parseInt(max.value, 10) - 1;
  }

  m.innerHTML = min.value;

}

function rangeReviewMax() {

  var min = document.getElementById('rangeReviewMin');
  var max = document.getElementById('rangeReviewMax');
  var m = document.getElementById('reviewMax');

  if (parseInt(max.value, 10) <= parseInt(min.value, 10)) {
    max.value = parseInt(min.value, 10) + 1;
  }

  m.innerHTML = max.value;

}

function rangeYearMin() {

  var min = document.getElementById('rangeYearMin');
  var max = document.getElementById('rangeYearMax');
  var m = document.getElementById('yearMin');

  if (parseInt(min.value, 10) >= parseInt(max.value, 10)) {
    min.value = parseInt(max.value, 10) - 1;
  }

  m.innerHTML = min.value;

}

function rangeYearMax() {

  var min = document.getElementById('rangeYearMin');
  var max = document.getElementById('rangeYearMax');
  var m = document.getElementById('yearMax');

  if (parseInt(max.value, 10) <= parseInt(min.value, 10)) {
    max.value = parseInt(min.value, 10) + 1;
  }

  m.innerHTML = max.value;

}
