
$(document).ready(function(){

  var nbEPP = 15;
  var movies = [];
  var filtred = [];
  var filtre = false;
  var page = 1;
  var load = false;
  var sortReview = false;
  var sortYear = false;
  var sortName = false;

  function drawResult(data, index) {
    var html = '<div class="movie-global">' +
               '<a class="minia-global" href="/movie/' + data[index].imdbid + '">' +
               '<div class="movie-img">';
    if (data[index].poster !== 'N/A' && data[index].viewed === true)
      html += '<img onerror="altImg(event)" class="movie-view" src="' + data[index].poster + '" />';
    else if (data[index].poster !== 'N/A')
      html += '<img onerror="altImg(event)" src="' + data[index].poster + '" />';
    else
      html += '<div class="movie-no-img"><i class="far fa-image"></i></div>';
    html += '</div>' +
            '<div>';
    if (data[index].viewed === true)
      html += '<p class="movie-title">' + data[index].title + ' <span class="movie-view-text">Viewed</span></p>';
    else
      html += '<p class="movie-title">' + data[index].title + '</p>';
    html += '<p class="movie-year">' + data[index].year + ' - <i class="fas fa-star"></i> ' + data[index].score + '/10</p>' +
            '</div>' +
            '</a>' +
            '</div>';
    $(".movies").append(html);
  }

  $.ajax({
    type: 'post',
    url: '/list',
    dataType: 'text'
  })
  .done(function(data){
    data = JSON.parse(data);
    if (data.length > 0) {
      movies = data;
      load = true;
      $(".movies").html('');

      var i = 0;
      while (i < nbEPP && i < data.length) {
        drawResult(data, i);
        i++;
      }
    }
    else {
      $(".movies").html("<p>No results found, retry later...</p>");
    }
  });

  $(window).scroll(function() {
    if(($(window).scrollTop() + $(window).height() + 10) >= $(document).height()) {
      var start = page * nbEPP;
      var end = start + nbEPP;
      if (filtre === true) {
        while (start < end && start < filtred.length) {
          drawResult(filtred, start);
          start++;
        }
      }
      else {
        while (start < end && start < movies.length) {
          drawResult(movies, start);
          start++;
        }
      }
      page++;
    }
  });

  $( ".btn-filter" ).click(function() {
    if (load === true) {
      filtred = [];
      $(".movies").html("");
      var rMin = document.getElementById('rangeReviewMin').value;
      var rMax = document.getElementById('rangeReviewMax').value;
      var yMin = document.getElementById('rangeYearMin').value;
      var yMax = document.getElementById('rangeYearMax').value;
      var genre = document.getElementById('genre').value;
      filtred = movies.filter((m) => {
        return (m.genreList.map(g => g.toLowerCase()).includes(genre) || genre === "all") && m.score <= rMax && m.score >= rMin &&
        m.year <= yMax && m.year >= yMin;
      })
      var j = 0;
      while (j < nbEPP && j < filtred.length) {
        drawResult(filtred, j);
        j++;
      }
      if (filtred.length === 0) {
        $(".movies").html("<p>No results found ...</p>");
      }
      page = 1;
      filtre = true;
    }
  });

  function sortManager(tmp) {
    page = 1;
    $(".movies").html('<div class="lds-dual-ring"></div>');
    $(".movies").html('');
    var i = 0;
    while (i < nbEPP && i < tmp.length) {
      drawResult(tmp, i);
      i++;
    }
  }

  $( ".sort-btn-name" ).click(function() {

    if (filtre === true) {
      if (sortName === false) {
        var tmp = filtred;
        tmp.sort(function(a, b){return a.title.localeCompare(b.title)});
        sortName = true;
        sortManager(tmp);
      } else {
        var tmp = filtred;
        tmp.sort(function(a, b){return b.title.localeCompare(a.title)});
        sortName = false;
        sortManager(tmp);
      }
    } else {
      if (sortName === false) {
        var tmp = movies;
        tmp.sort(function(a, b){return a.title.localeCompare(b.title)});
        sortName = true;
        sortManager(tmp);
      } else {
        var tmp = movies;
        tmp.sort(function(a, b){return b.title.localeCompare(a.title)});
        sortName = false;
        sortManager(tmp);
      }
    }

  });

  $( ".sort-btn-review" ).click(function() {

    if (filtre === true) {
      if (sortReview === false) {
        var tmp = filtred;
        tmp.sort(function(a, b){return b.score - a.score});
        sortReview = true;
        sortManager(tmp);
      } else {
        var tmp = filtred;
        tmp.sort(function(a, b){return a.score - b.score});
        sortReview = false;
        sortManager(tmp);
      }
    } else {
      if (sortReview === false) {
        var tmp = movies;
        tmp.sort(function(a, b){return b.score - a.score});
        sortReview = true;
        sortManager(tmp);
      } else {
        var tmp = movies;
        tmp.sort(function(a, b){return a.score - b.score});
        sortReview = false;
        sortManager(tmp);
      }
    }

  });

  $( ".sort-btn-year" ).click(function() {

    if (filtre === true) {
      if (sortYear === false) {
        var tmp = filtred;
        tmp.sort(function(a, b){return b.year - a.year});
        sortYear = true;
        sortManager(tmp);
      } else {
        var tmp = filtred;
        tmp.sort(function(a, b){return a.year - b.year});
        sortYear = false;
        sortManager(tmp);
      }
    } else {
      if (sortYear === false) {
        var tmp = movies;
        tmp.sort(function(a, b){return b.year - a.year});
        sortYear = true;
        sortManager(tmp);
      } else {
        var tmp = movies;
        tmp.sort(function(a, b){return a.year - b.year});
        sortYear = false;
        sortManager(tmp);
      }
    }

  });

});

function altImg(e) {
  e.target.outerHTML = '<div class="movie-no-img"><i class="far fa-image"></i></div>';
}
