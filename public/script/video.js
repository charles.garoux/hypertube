$(document).ready(function(){

  var title = $("#video-title")[0].textContent;
  var imdb = $("#addComment #imdbid")[0].value;
  var date = $(".video-title")[0].dataset.year;
  var nbS = $("#series-nb")[0].value;

  $.ajax({
    type: 'post',
    url: '/movie/list',
    data: {imdbid: imdb, title: title, date: date},
    dataType: 'text'
  })
  .done(function(data){
    data = JSON.parse(data);
    $(".video-list").html('');

    if (nbS > 0) {

      $(".video-list").append(
        '<div class="btn-season"></div>'
      );

      var s = 0;

      while (s < nbS) {

        var sa = (s + 1);
        if (sa < 10) {
          sa = '0' + sa;
        }

        $(".btn-season").append(
          '<input class="season-val" type="button" value="s' + sa + '"/>'
        );
        s++;
      }

    }

    if (data.length > 0) {

      var i = 0;

      while (i < data.length) {
        let inputClass = 'magnet-form';
        let readySpan = '';
        if (data[i].ready) {
          readySpan = '<span class="ready-torrent-text">Ready</span>';
          inputClass += ' torrent-ready';
        }
        $(".video-list").append(
          '<form class="' + inputClass + '" action="" method="post">' +
          '<input type="hidden" name="magnet" value="' + data[i].magnet + '"/>' +
          '<div class="film-logo">' +
          '<i class="fas fa-film"></i>' +
          '<input class="magnet-link" data-ready="' + data[i].ready + '" type="submit" value="' + data[i].title + ' - ' + data[i].source + ' - ' + (data[i].size || "Unknown size") + ' - ' + data[i].seeders + ' seeds" />' +
          readySpan +
          '</div>' +
          '</form>'
        );

        i++;
      }

    }
    else {
      //$(".video-list").html("<p>No results found, retry later...</p>");

      var title = $("#video-title").html();
      $(".video-list").html("");

      if (title === "Star Wars: The Last Jedi") {
        $(".video-list").append(
          '<form class="" onclick="starwars(event);" action="" method="post">' +
          '<div class="film-logo">' +
          '<i class="fas fa-film"></i>' +
          '<input class="magnet-link" type="submit" value="' + "Star Wars: The Last Jedi 2008" + ' - ' + "YouTube" + ' - ' + "Unknown size" + ' - ' + '1' + ' seeds" />' +
          '</div>' +
          '</form>'
        );
      }

      $(".video-list").append(
        '<form class="" onclick="trololo(event);" action="" method="post">' +
        '<div class="film-logo">' +
        '<i class="fas fa-film"></i>' +
        '<input class="magnet-link" type="submit" value="' + "Trolololo" + ' - ' + "YouTube" + ' - ' + "Unknown size" + ' - ' + '1' + ' seeds" />' +
        '</div>' +
        '</form>'
      );

    }


  });


  $('body').on('click', '.season-val', function(){

    $(".video-list").html('<div class="lds-dual-ring-list"></div>');

    $.ajax({
      type: 'post',
      url: '/movie/list/search',
      data: {imdbid: imdb, title: title, search: $(this)[0].value},
      dataType: 'text'
    })
    .done(function(data){

      data = JSON.parse(data);

      $(".video-list").html('');

      if (nbS > 0) {

        $(".video-list").append(
          '<div class="btn-season"></div>'
        );

        var s = 0;

        while (s < nbS) {

          var sa = (s + 1);
          if (sa < 10) {
            sa = '0' + sa;
          }

          $(".btn-season").append(
            '<input class="season-val" type="button" value="s' + sa + '"/>'
          );
          s++;
        }

      }

      if (data.length > 0) {

        var i = 0;

        while (i < data.length) {

          $(".video-list").append(
            '<form class="magnet-form" action="" method="post">' +
            '<input type="hidden" name="magnet" value="' + data[i].magnet + '"/>' +
            '<div class="film-logo">' +
            '<i class="fas fa-film"></i>' +
            '<input class="magnet-link" type="submit" value="' + data[i].title + ' - ' + data[i].source + ' - ' + (data[i].size || "Unknown size") + ' - ' + data[i].seeders + ' seeds" />' +
            '</div>' +
            '</form>'
          );

          i++;
        }

      }
      else {
        $(".video-list").html("<p>No results found, retry later...</p>");
      }


    });

  });

});

function trololo(ev) {
  ev.preventDefault();
  $("#close-video").html('<iframe width="560" height="315" src="https://www.youtube.com/embed/oavMtUWDBTM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
  $("#video-modal").css("display", "block");
}

function starwars(ev) {
  ev.preventDefault();
  $("#close-video").html('<iframe width="560" height="315" src="https://www.youtube.com/embed/u69sCCWcdSo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
  $("#video-modal").css("display", "block");
}
