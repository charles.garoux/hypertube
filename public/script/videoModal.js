var socket = io.connect('http://' + window.location.host);
var subs = [];
var dlTime = 0;
var convPct = 0;
var userLang = $('#video-modal').data('lang');
var fileSelected = [];

socket.on('torrentReady', (data) => {
    files = data.msg
    if (data.msg.code != 1)
    {
      $('.modal-content p, .modal-content div').remove()
      files.forEach(file => {
        $('.modal-content').append(`<div onclick='selectFile(this)' data-id='${file.id}' id='file${data.tid}${file.id}' data-tid='${data.tid}' class="torrent-load">
            <p class="loading-title">${file.name}</p>
          </div>`);
      });
    }
})

socket.on('downloadStarted', (data) => {
  updateLoading(data);
})

function updateLoading(data) {
  if (!fileSelected.includes(`${data.tid}-${data.index}`))
    return;
  if ($(`.modal-content #file${data.tid}${data.index} .loading`).length == 0)
  {
    $(`.modal-content #file${data.tid}${data.index}`).append(`<div class="lds-dual-ring"></div>
    <p class="loading">0 %</p>`);
  }
  else if (data.dlPct)
    $(`.modal-content #file${data.tid}${data.index} .loading`).html(`<p class="loading">${data.dlPct} %</p>`)
}

socket.on('streamError', (data) => {
  if ($(`#file${data.tid}${data.index} .loading-message`).length == 0)
    $(`.modal-content #file${data.tid}${data.index}`).append(`<p class="loading-message">This file may not be streamable</p>`)
  else
    $(`#file${data.tid}${data.id} .loading-message`).html(`<p class="loading-message">This file may not be streamable</p>`)
  if ($(`#file${data.tid}${data.id} .play-btn`).length == 0 && data.ext == '.mp4')
    $(`#file${data.tid}${data.id}`).append(`<button onclick='playBtn(this)' class='play-btn' data-master='${data.master}' data-source='${data.source}' data-tid='${data.tid}' data-id='${data.id}' data-src='/torrent/play/${data.tid}/${data.index}'>PLAY ANYWAY <i class="fas fa-play"></i></button>`);
})

socket.on('downloadProgress', (data) => {
  updateLoading(data)

  //for video modal
  dlTime  = data.dlTime;
  dlPct = data.dlPct;
  $(`#pBar${data.tid}${data.index}`).html(`<p class='bars' id='pBar${data.tid}${data.index}'>Download: ${dlPct} %</p>`)
})

socket.on('convStarted', (data) => {
  updateLoading(data)
  if ($(`#file${data.tid}${data.index} .loading-message`).length == 0)
    $(`.modal-content #file${data.tid}${data.index}`).append(`<p class="loading-message">Conversion Started</p>`)
})


socket.on('convProgress', (data) => {
  //console.log('convPct', data)
  updateLoading(data)
  if ($(`#file${data.tid}${data.index} .loading-message`).length == 0)
    $(`#file${data.tid}${data.index}`).append(`<p class="loading-message">Conversion Started</p>`)
  else
    $(`#file${data.tid}${data.index} .loading-message`).html(`<p class="loading-message">Conversion Started</p>`)
  convPct = data.convPct
  $(`#cBar${data.tid}${data.index}`).html(`<p class='bars' id='cBar${data.tid}${data.index}'>Conversion: ${data.convTime} sec</p>`)
})

socket.on('convFailed', (data) => {
  $(`.modal-content #file${data.tid}${data.index} .loading-message`).html(`<p class="loading-message">Conversion Failed</p>`)
})

socket.on('readyToStream', (data) => {
  //console.log(data)
  if ($(`.modal-content #file${data.tid}${data.index} .play-btn`).length == 0)
    $(`.modal-content #file${data.tid}${data.index}`).append(`<button onclick='playBtn(this)' class='play-btn' data-master='${data.master}' data-source='${data.source}' data-tid='${data.tid}' data-id='${data.index}' data-src='/torrent/play/${data.tid}/${data.index}'>PLAY <i class="fas fa-play"></i></button>`);
  subs = data.subs;
  })

  socket.on('subsAdded', (s) => {
    //console.log('New subs')
    s.default = (s.langcode == userLang)
    if (s.langcode != '' && $(`#video track[srclang=${s.langcode}]`).length == 0)
    {
      $('#video').append(`<track default="${s.default}" kind="captions"
        srclang="${s.langcode}" label="${s.lang}"
        src="/${s.path}"/>`);
    }
    selectSubtitles()
  })

function selectFile(e) {
  //$.get(`/torrent/files/${e.dataset.tid}/${e.dataset.id}`, (data) => {})
  socket.emit('selectFile', e.dataset.id);
  fileSelected.push(`${e.dataset.tid}-${e.dataset.id}`);
}

$(document).on('submit', '.magnet-form', (e) => {
    e.preventDefault();
    selectMagnet(e.target[0].value)
})

function selectMagnet(magnet)
{
    //console.log(magnet)
    modal.style.display = "block";
    $('.modal-content p, .modal-content div').remove()
    $('.modal-content').append(`<div class='torrent-load'><p class="loading-title">Loading files in torrent</p>
        <div class="lds-dual-ring"></div></div>`)
    socket.emit('magnet', {magnet: magnet})
}

function playBtn(btn) {
  let e = {};
  e.target = btn;
  //console.log(e.target.dataset)
  if (e.target.dataset.source || e.target.dataset.master)
  {
    var dlTime = 0;
    var convPct = 0;
    $('.bars').remove();
    $('#video').after(`<p class='bars' id='pBar${e.target.dataset.tid}${e.target.dataset.id}'></p>`);
    $('#video').after(`<p class='bars' id='cBar${e.target.dataset.tid}${e.target.dataset.id}'></p>`);
    //window.location.href = e.target.dataset.src;
    if (e.target.dataset.source != 'undefined')
      loadVideoModal(e.target.dataset.source, 1);
    else
      loadVideoModal(e.target.dataset.master, 0);
  }
}



var modal = document.getElementById("modal");
var videoModal = document.getElementById("video-modal");

var close = document.getElementsByClassName("close")[0];
var closeLoad = document.getElementsByClassName("close-load")[0];



closeLoad.onclick = function() {
  modal.style.display = "none";
  document.body.style.overflow = 'auto';
  $('.modal-content *').remove()
}


close.onclick = function() {

  videoModal.style.display = "none";
  document.body.style.overflow = 'auto';
  $('.modal-content *').remove()
  var video = document.getElementById('video');
  if (video)
    video.remove();

  $('.video-modal-content .close').after(`<video onerror='vError(event)' controls class='video-player' id="video"></video>`)
}

function loadVideoModal(url, isMp4) {
  var imdbid = $('#video-modal').data('imdbid');
  $.get(`/movie/setSeen/${imdbid}`, (d) => {
  });
  $('.video-modal-content .error').remove();
  var video = document.getElementById('video');
  video.dataset.url = url
  video.dataset.isMp4 = isMp4
  modal.style.display = "none";
  videoModal.style.display = "block";
  $('#video track').remove();
  if (subs != [])
  {
    subs.forEach((s) => {
        s.default = (s.langcode == userLang)
        let sub = $(video).append(`<track default="${s.default}" kind="captions"
        srclang="${s.langcode}" label="${s.lang}"
        src="/${s.path}"/>`);

    })
}
selectSubtitles()
if (isMp4)
{
  video.src = `/${url}`;
  video.type = 'video/mp4';

  /*$('#video').on('timeupdate', (ev) => {
    if (dlTime >= 60 && ev.target.currentTime > dlTime)
    {
      //console.log('too far')
      //console.log(ev.target.currentTime, dlTime)
      ev.target.currentTime = dlTime - 60;
    }
  })*/
} else {

        if(Hls.isSupported()) {
          var hls = new Hls();
          hls.loadSource(`/${url}`);

          hls.attachMedia(video);

          hls.on(Hls.Events.MANIFEST_PARSED,function() {
            video.play();
        });
      }
}

}

function selectSubtitles() {
let i = $('#video')[0].textTracks.length;
while (--i >= 0)
{
  let t = $('#video')[0].textTracks[i]
  if (t.language == userLang)
    t.mode = 'showing'
  else
    t.mode = 'hidden'
}
}

function vError (ev) {
  //console.log('video error', ev)
  //ev.target.currentTime = 0;
  $('.video-modal-content').append(`<p class='error'>An error has been detected, you can <a src='#' onclick='reloadPlayer()'>RELOAD PLAYER</a></p>`);
}

function reloadPlayer()
{
  var video = document.getElementById('video');
  video.remove();

  $('.video-modal-content .close').after(`<video onerror='vError(event)' controls class='video-player' id="video"></video>`)
  loadVideoModal(video.dataset.url, video.dataset.isMp4)
}
