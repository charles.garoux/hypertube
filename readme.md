# Hypertube

Dernier **projet scolaire** de la branche Web avant le premier stage de l'école [42 Lyon](https://www.42lyon.fr/), il doit être fait en équipe de trois à quatre étudiants.  
Je suis arrivé en cours de route avec un camarade sur le projet de deux autres étudiants et je découvrais cette technologie et ces nouveaux paradigme de programmation.

## Objectif

Dernier projet de sa série, le projet Hypertube nous invite à découvrir une catégorie d'outil extrêmement puissante : les frameworks MVC.  
Nous apprendrons à manipuler un MVC, dans le langage de notre choix, pour réaliser un site de streaming de vidéo téléchargées via le protocole BitTorrent.

## Résultat
D’abord, je me suis concentré sur l’organisation de l'équipe notamment grâce à un schéma de l’architecture de l’application et l’utilisation de Zenkit, un outil de gestion de projet.  
Ensuite, j’ai développé un système de cache de données pour les films et séries avec les torrents les plus populaires.  
Pour finir, j’ai travaillé avec un équipier plus expérimenté sur la technologie pour refactoriser une majeure partie du projet pour éviter une dette technique.  
  
Le projet a été validé par 5 correcteurs durant la soutenance avec mon équipe.

# Readme du rendu de projet

Create a symlink to a folder with space
ex: ln -s /tmp video

Launch the server and wait for the database to populate
