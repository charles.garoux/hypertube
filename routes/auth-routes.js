const colors = require('colors/safe');
const router = require('express').Router();
const passport = require('passport');
const auth = require('../config/authcheck');
const Middleware = require('../Middlewares');

router.get('/github', Middleware.Auth.notCheck, passport.authenticate('github'), (req, res) => {
  console.log('test');
});

router.get('/github/redirect', Middleware.Auth.notCheck, passport.authenticate('github'), (req, res) => {
  res.redirect('/');
});

router.get('/google', Middleware.Auth.notCheck, passport.authenticate('google', {
  scope:['profile']
}));

router.get('/google/redirect', Middleware.Auth.notCheck, passport.authenticate('google'), (req, res) => {
  res.redirect('/');
});

router.get('/42', Middleware.Auth.notCheck, passport.authenticate('42', {
  scope:['public']
}));

router.get('/42/redirect', Middleware.Auth.notCheck, passport.authenticate('42'), (req, res) => {
  res.redirect('/');
});

router.get('/twitter', Middleware.Auth.notCheck, passport.authenticate('twitter'));

router.get('/twitter/redirect', Middleware.Auth.notCheck, passport.authenticate('twitter'), (req, res) => {
  res.redirect('/');
});

router.get('/logout', Middleware.Auth.check, (req, res) => {
  console.log("User " + colors.yellow(req.user.username) + " disconnect");
  req.logout();
  res.redirect('/login');
});

module.exports = router;
