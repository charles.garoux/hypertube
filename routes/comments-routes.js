const router = require('express').Router();
const niv = require('node-input-validator');
const auth = require('../config/authcheck');
const CommentsManager = require('../Controllers/Comments');
const Middleware = require('../Middlewares');

//Get comments for a movie
router.post('/select/:imdbid', Middleware.Auth.check, async(req, res) => {
  const v = new niv.Validator(req.params, {
    imdbid: 'required|imdbid'
  });

  const matched = await v.check();

  if (!matched) {
    return res.status(422).send(v.errors);
  }

  let coms = await CommentsManager.getComments(req.params.imdbid)
  res.send({data: coms})
});

//Add comment to movie
router.post('/add/:imdbid', Middleware.Auth.check, async (req, res) => {
  const v = new niv.Validator(req.body, {
    comment: 'required|string:minLength:2|maxLength:200'
  });
  const vParams = new niv.Validator(req.params, {
    imdbid: 'required|imdbid'
  });

  const matched = await v.check() && await vParams.check();

  if (!matched) {
    //console.log(err)
    let err = v.errors
    return res.status(422).send(err);
  }

  let data = await CommentsManager.addComment(req.body.comment, req.params.imdbid, req.user._id)
  res.send(data)
});

module.exports = router;
