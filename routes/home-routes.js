const router = require('express').Router();
const auth = require('../config/authcheck');
const Home = require('../Controllers/Home');
const Middleware = require('../Middlewares');

//Main page
router.get('/', Middleware.Auth.check, (req, res) => {
    res.render('home', {user: req.user, query: ''});
  });

//List main page (ajax)
router.post('/list', Middleware.Auth.check, (req, res) => {
    Home.listWithViewed(req.user._id, (list) => {
      res.send(list)
    })
  });

  module.exports = router;
