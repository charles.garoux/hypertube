const router = require('express').Router();
const User = require('../Schemas/user.js');
const niv = require('node-input-validator');
const auth = require('../config/authcheck');
const Middleware = require('../Middlewares');

router.get('/:username', Middleware.Auth.check, async (req, res) => {
  const v = new niv.Validator(req.params, {
    username: 'required|string'
  });

  const matched = await v.check();

  if (!matched) {
    return res.status(422).send(v.errors);
  }

  User.findOne({username: req.params.username}).then((user) => {

    if (user) {

      var viewed = user.views;
      var sortViewed = viewed.sort(function(a, b){return b.date - a.date});

      res.render('profile', {user: req.user, userProfil: user, viewed: sortViewed, query: ''});
    }
    else {
      res.status(404);
      res.render('404', {user: req.user, query: ''});
    }

  });

});

module.exports = router;
