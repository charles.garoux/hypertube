const router = require('express').Router();
const auth = require('../config/authcheck');
const RegisterManager = require('../Controllers/Register')
const { Validator } = require('node-input-validator');
const bodyParser = require('body-parser');
const urlencodedparser = bodyParser.urlencoded({ extended: false });
const colors = require('colors/safe');
const Middleware = require('../Middlewares');

router.get('/login', Middleware.Auth.notCheck, (req, res) => {
  res.render('login', {user: req.user, query: '', error: ''});
});

router.get('/register', Middleware.Auth.notCheck, (req, res) => {
  res.render('register', {user: req.user, query: '', error: '', confirm: ''});
});

router.get('/forgot', Middleware.Auth.notCheck, (req, res) => {
  res.render('forgot', {user: req.user, query: '', error: '', confirm: ''});
});

router.post('/login', auth.notAuthCheck, urlencodedparser, (req, res) => {

  const v = new Validator(req.body, {
    username: 'required|minLength:4|maxLength:20|string',
    password: 'required|minLength:3|maxLength:50|string'
  });

  v.check().then((matched) => {

    if (!matched) {
      for (let [rule, error] of Object.entries(v.errors))
        return res.render('login', {user: req.user, query: '', error: error.message});
    }
    else {

      let result = RegisterManager.login(req.body.username, req.body.password);
      result.then((result) => {
        if (result.code)
          res.render('login', {user: req.user, query: '', error: result.error});
        else {
          req.login(result.user, function(err) {
            if (err)
              //console.log(err);
            console.log("User " + colors.yellow(result.user.username) + " is now connected");
            res.redirect('/');
          });
        }
      })

    }

  });

});

router.post('/register', Middleware.Auth.notCheck, urlencodedparser, (req, res) => {

  const v = new Validator(req.body, {
    firstname: 'required|minLength:3|maxLength:20|string',
    lastname: 'required|minLength:3|maxLength:20|string',
    email: 'required|email|string',
    username: 'required|minLength:4|maxLength:20|string',
    pass: 'required|minLength:5|maxLength:50|string|password',
    cpass: 'required|minLength:5|maxLength:50|string|password'
  });

  v.check().then((matched) => {

    if (!matched) {

      for (let [rule, error] of Object.entries(v.errors))
        return res.render('register', {user: req.user, query: '', error: error.message, confirm: ''});

    }
    else {

      RegisterManager.register(req.body.firstname, req.body.lastname, req.body.email, req.body.username, req.body.pass, req.body.cpass, (result) => {
       if (result.code)
         res.render('register', {user: req.user, query: '', error: result.error, confirm: ''});
       else
         res.render('register', {user: req.user, query: '', error: '', confirm: result.confirm});
      })

    }

  });

});

router.get('/confirm/:token', Middleware.Auth.notCheck, (req, res) => {
  RegisterManager.confirmMail(req.params.token, (result) => {
    if (result.code)
    {
      res.status(404);
      res.render('404', {user: req.user, query: ''});
    }
    else
      res.render('register', {user: req.user, query: '', error: '', confirm: result.confirm});
   })
});

router.post('/forgot', Middleware.Auth.notCheck, urlencodedparser, (req, res) => {
  RegisterManager.forgotPassword(req.body.email, (result) => {
    if (result.code)
      res.render('forgot', {user: req.user, query: '', error: result.error, confirm: ''});
    else
      res.render('forgot', {user: req.user, query: '', error: '', confirm: result.confirm});
   })
});

router.get('/reset/:token', Middleware.Auth.notCheck, (req, res) => {
  RegisterManager.prepareResetPassword(req.params.token, (result) => {
    if (result.code)
    {
      res.status(404);
      res.render('404', {user: req.user, query: ''});
    }
    else
      res.render('reset', {user: req.user, query: '', token: req.params.token, error: '', confirm: ''});
   })
});

router.post('/reset', Middleware.Auth.notCheck, urlencodedparser, (req, res) => {

  const v = new Validator(req.body, {
    token: 'required|string',
    pass: 'required|minLength:5|maxLength:50|string|password',
    cpass: 'required|minLength:5|maxLength:50|string|password'
  });

  v.check().then((matched) => {

    if (!matched) {

      for (let [rule, error] of Object.entries(v.errors))
        return res.render('reset', {user: req.user, query: '', token: req.body.token, error: error.message, confirm: ''});

    }
    else {

      RegisterManager.resetPassword(req.body.pass, req.body.cpass, req.body.token, (result) => {
        if (result.code)
        {
          if (result.error === 404)
          {
            res.status(404);
            res.render('404', {user: req.user, query: ''});
          }
          else
            res.render('reset', {user: req.user, query: '', token: req.body.token, error: result.error, confirm: ''});
        }
        else
          res.render('reset', {user: req.user, query: '', token: '', error: '', confirm: result.confirm});
       })

    }

  });

});

module.exports = router;
