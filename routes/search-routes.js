const router = require('express').Router();
const bodyParser = require('body-parser');
const urlencodedparser = bodyParser.urlencoded({ extended: false });
const niv = require('node-input-validator');
const auth = require('../config/authcheck');
const SearchManager = require('../Controllers/Search')
const Middleware = require('../Middlewares');

//Search
router.post('/', Middleware.Auth.check, urlencodedparser, async function(req, res){
  const v = new niv.Validator(req.body, {
    query: 'required|string'
  });

  const matched = await v.check();

  if (!matched) {
    res.render('404', {user: req.user, query: ''});
  }

    var regex = /tt\d{7,8}$/;
    var regMagnet = /magnet:\?xt=urn:btih\::*.*/;

    if (regex.test(req.body.query)) {
      res.redirect('/movie/' + req.body.query);
    }
    else if (regMagnet.test(req.body.query)) {
      res.render('magnet', {user: req.user, query: req.body.query, userLang: req.user.lang});
    }
    else {
      res.render('search', {user: req.user, query: req.body.query});
    }
});

//Search results (ajax)
router.post('/list', Middleware.Auth.check, async function(req, res){
  const v = new niv.Validator(req.body, {
    query: 'required|string'
  });

  const matched = await v.check();

  if (!matched) {
    return res.status(422).send(v.errors);
  }

  SearchManager.getResults(req.body.query, req.user._id)
      .then(results => {
        res.send(results)
      }).catch(error => {
        res.send([]);
      });
});

module.exports = router;