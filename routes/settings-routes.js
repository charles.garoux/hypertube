const router = require('express').Router();
const bodyParser = require('body-parser');
const SettingsManager = require('../Controllers/Settings')
const { Validator } = require('node-input-validator');
const auth = require('../config/authcheck');
const urlencodedparser = bodyParser.urlencoded({ extended: false });
const Middleware = require('../Middlewares');

//Settings page
router.get('/', Middleware.Auth.check, (req, res) => {
  res.render('settings', {user: req.user, query: '', error: '', errorPass: '', errorPic: '', confirm: '', confirmPass: '', confirmPic: ''});
});

//Edit settings
router.post('/', Middleware.Auth.check, urlencodedparser, (req, res) => {

  const v = new Validator(req.body, {
    lang: 'required|string|lang',
    email: 'required|email|string',
    username: 'required|minLength:4|maxLength:20|string',
    firstname: 'required|minLength:3|maxLength:20|string',
    lastname: 'required|minLength:3|maxLength:20|string'
  });

  v.check().then((matched) => {
    if (!matched) {
      for (let [rule, error] of Object.entries(v.errors))
        return res.render('settings', {user: req.user, query: '', error: error.message, errorPass: '', errorPic: '', confirm: '', confirmPass: '', confirmPic: ''});
    }
    else {
      SettingsManager.changeSettings(req.body.lang, req.body.username, req.body.firstname, req.body.lastname, req.body.email, req.user, (result) => {
       if (result.code) {
         if (result.error === 404)
         {
           res.status(404);
           res.render('404', {user: req.user, query: ''});
         }
         else {
           res.render('settings', {user: req.user, query: '', error: result.error, errorPass: '', errorPic: '', confirm: '', confirmPass: '', confirmPic: ''});
         }
       }
       else {
         req.user = result.user;
         res.render('settings', {user: req.user, query: '', error: '', errorPass: '', errorPic: '', confirm: result.confirm, confirmPass: '', confirmPic: ''});
       }
      })
    }
  });

});

//Edit settings Oauth
router.post('/oauth', Middleware.Auth.check, urlencodedparser, (req, res) => {

  const v = new Validator(req.body, {
    lang: 'required|string|lang',
    username: 'required|minLength:4|maxLength:20|string',
    firstname: 'required|minLength:3|maxLength:20|string',
    lastname: 'required|minLength:3|maxLength:20|string'
  });

  v.check().then((matched) => {

    if (!matched) {

      for (let [rule, error] of Object.entries(v.errors))
        return res.render('settings', {user: req.user, query: '', error: error.message, errorPass: '', errorPic: '', confirm: '', confirmPass: '', confirmPic: ''});

    }
    else {

      SettingsManager.changeOauthSettings(req.body.lang, req.body.username, req.body.firstname, req.body.lastname, req.user, (result) => {
       if (result.code) {
         if (result.error === 404)
         {
           res.status(404);
           res.render('404', {user: req.user, query: ''});
         }
         else {
           res.render('settings', {user: req.user, query: '', error: result.error, errorPass: '', errorPic: '', confirm: '', confirmPass: '', confirmPic: ''});
         }
       }
       else {
         req.user = result.user;
         res.render('settings', {user: req.user, query: '', error: '', errorPass: '', errorPic: '', confirm: result.confirm, confirmPass: '', confirmPic: ''});
       }
      })

    }

  });

});

//Edit password
router.post('/password', Middleware.Auth.check, urlencodedparser, (req, res) => {

  const v = new Validator(req.body, {
    pass: 'required|minLength:5|maxLength:50|string|password',
    cpass: 'required|minLength:5|maxLength:50|string|password'
  });

  v.check().then((matched) => {
    if (!matched) {
      for (let [rule, error] of Object.entries(v.errors))
        return res.render('settings', {user: req.user, query: '', error: '', errorPass: error.message, errorPic: '', confirm: '', confirmPass: '', confirmPic: ''});
    }
    else {
      SettingsManager.changePasswordSettings(req.body.pass, req.body.cpass, req.user,(result) => {
       if (result.code) {
         if (result.error === 404)
         {
           res.status(404);
           res.render('404', {user: req.user, query: ''});
         }
         else {
           res.render('settings', {user: req.user, query: '', error: '', errorPass: result.error, errorPic: '', confirm: '', confirmPass: '', confirmPic: ''});
         }
       }
       else {
         req.user = result.user;
         res.render('settings', {user: req.user, query: '', error: '', errorPass: '', errorPic: '', confirm: '', confirmPass: result.confirm, confirmPic: ''});
       }
      })
    }
  });

});

//Edit profil picture
router.post('/picture', Middleware.Auth.check, urlencodedparser, (req, res) => {

  const v = new Validator(req.body, {
    picture: 'required|string',
    base64: 'required|string|pictureFormat|pictureSize'
  });

  v.check().then((matched) => {
    if (!matched) {
      for (let [rule, error] of Object.entries(v.errors))
        return res.render('settings', {user: req.user, query: '', error: '', errorPass: '', errorPic: error.message, confirm: '', confirmPass: '', confirmPic: ''});
    }
    else {
      SettingsManager.changePictureSettings(req.body.base64, req.user, (result) => {
       if (result.code) {
         if (result.error === 404)
         {
           res.status(404);
           res.render('404', {user: req.user, query: ''});
         }
         else {
           res.render('settings', {user: req.user, query: '', error: '', errorPass: '', errorPic: result.error, confirm: '', confirmPass: '', confirmPic: ''});
         }
       }
       else {
         req.user = result.user;
         res.render('settings', {user: req.user, query: '', error: '', errorPass: '', errorPic: '', confirm: '', confirmPass: '', confirmPic: result.confirm});
       }
      })
    }
  });

});

module.exports = router;
