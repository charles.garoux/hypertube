const router = require('express').Router();
const bodyParser = require('body-parser');
const urlencodedparser = bodyParser.urlencoded({ extended: false });
const niv = require('node-input-validator');
const auth = require('../config/authcheck');
const VideoManager = require('../Controllers/Video');
const Middleware = require('../Middlewares');

//Main video page
router.get('/:imdbid', Middleware.Auth.check, async(req, res) => {

  const v = new niv.Validator(req.params, {
    imdbid: 'required|imdbid'
  });

  const matched = await v.check();

  if (!matched) {
    return res.render('404', {user: req.user, query: ''});
  }

  VideoManager.getMovieInfo(req.params.imdbid, req.user._id)
    .then((results) => {
      res.render('video', {user: req.user, query: '', movieinfos: results, imdbid: req.params.imdbid, userLang: req.user.lang});
    })
    .catch((err) => {
      if (err.error)
      {
        //console.log(err.error.Error);
      }
      else
      {
          //console.log(err);
      }
      res.render('404', {user: req.user, query: ''});
    })
});

//Set movie seen
router.get('/setSeen/:imdbid', Middleware.Auth.check, async (req, res) => {
  const v = new niv.Validator(req.params, {
    imdbid: 'required|imdbid'
  });

  const matched = await v.check();

  if (!matched) {
    return res.render('404', {user: req.user, query: ''});
  }

  VideoManager.setMovieSeen(req.params.imdbid, req.user._id);
  res.send('Ok')
})

//Get torrent list in ajax
router.post('/list', Middleware.Auth.check, urlencodedparser, async (req, res) => {
  const v = new niv.Validator(req.body, {
    imdbid: 'required|imdbid',
    title: 'required|string'
  });

  const matched = await v.check();

  if (!matched) {
    return res.status(422).send(v.errors);
  }

  VideoManager.getMagnetList(req.body.imdbid, req.body.title, req.body.date, undefined, (list) => {
    res.send(list)
  })

});


//Search season of a serie (ajax)
router.post('/list/search', Middleware.Auth.check, urlencodedparser, async (req, res) => {
  const v = new niv.Validator(req.body, {
    search: 'required|string',
    title: 'required|string'
  });

  const matched = await v.check();

  if (!matched) {
    return res.status(422).send(v.errors);
  }

  VideoManager.getMagnetList(req.body.imdbid, req.body.title, undefined, req.body.search, (list) => {
    res.send(list)
  })

});

module.exports = router;