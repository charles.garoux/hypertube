const parseTorrent = require('parse-torrent')
var TorrentEngine = require('../Library/TorrentEngine')
const niv = require('node-input-validator');
const VideoManager = require('../Controllers/Video');

module.exports = function(io) {

torrents = [];

io.sockets.on('connection', (socket) =>
{
    socket.on('magnet', async (data) => {
        const v = new niv.Validator(data, {
            magnet: 'required|magnet'
        });

        const matched = await v.check();

        if (!matched) {
        return socket.emit('invMagnet');
        }

        if (data.magnet)
        {
            tid = parseTorrent(data.magnet).infoHash
        }
        else if (data.tid)
            tid = data.tid
    
        if (!torrents[tid])
            torrents[tid] = new TorrentEngine(data.magnet, '')
        else
            socket.emit('torrentReady', {msg: torrents[tid].getList(), tid: tid})


        socket.on('selectFile', (index) => {
            if (!torrents[tid])
                socket.emit('error')
            torrents[tid].startFile(index, (r) => {
                socket.emit('fileSelected')
            })
        })

        torrents[tid].on('torrentReady', (msg) => {
            socket.emit('torrentReady', {msg: msg, tid: tid})
        })

        torrents[tid].on('convStarted', (msg) => {
            socket.emit('convStarted', msg)
        })
        torrents[tid].on('convProgress', (msg) => {
            socket.emit('convProgress', msg)
        })
        torrents[tid].on('convFailed', (msg) => {
            socket.emit('convFailed', msg)
        })
        torrents[tid].on('readyToStream', (msg) => {
            socket.emit('readyToStream', msg)
        })
        torrents[tid].on('downloadStarted', (msg) => {
            socket.emit('downloadStarted', msg)
        })
        torrents[tid].on('downloadProgress', (msg) => {
            socket.emit('downloadProgress', msg)
        })
        torrents[tid].on('streamError', (msg) => {
            socket.emit('streamError', msg)
        })
        torrents[tid].on('subsAdded', (msg) => {
            socket.emit('subsAdded', msg)
        })
        socket.on('disconnect', () => {
            torrents[tid].removeAllListeners();
        })
    })
    })
}