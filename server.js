//Configs
require('./config/passport-config.js');
require('./config/validatorRules.js')

const colors = require('colors/safe');
const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const cookieSession = require('cookie-session');
const bodyParser = require('body-parser');
const TorrentManager = require('./Library/TorrentManager');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);
const fs = require('fs')
const servConfig = JSON.parse(fs.readFileSync('./config/server.config.json'))
const torrentConfig = JSON.parse(fs.readFileSync('./config/torrent-config.json'))
const randomBytes = require('randombytes');
const Crons = require('./Library/Crons');
const crons = new Crons();
const CacheEngine = require('./Library/CacheEngine');

//Paranoid keys for session
//Commented for dev environement
servConfig.server.sessions.keys = [randomBytes(2048).toString('base64'),randomBytes(2048).toString('base64'),randomBytes(2048).toString('base64'),randomBytes(2048).toString('base64')]

//Server initialisation
mongoose.connect(`mongodb+srv://${servConfig.database.username}:${servConfig.database.password}@${servConfig.database.server}/${servConfig.database.database}`, { useNewUrlParser: true, useUnifiedTopology: true }, () =>{
  console.log(colors.cyan("Connected to mongodb !"));
});

//Startup and recurrent tasks

crons.addCron('deleteOrphanTorrents', TorrentManager.deleteOrphanTorrents, 1000 * 60 * 60 * 24);
crons.addCron('deleteOldTorrents', TorrentManager.deleteOldTorrents, 1000 * 60 * 60 * 24);
crons.addCron('deleteOrphanEntries', TorrentManager.deleteOrphanEntries, 1000 * 60 * 60 * 24);

console.log('Think about creating a symlink of /video to a folder with some space')
console.log('Server initializing...')

//create upload folder
try
{
  fs.mkdirSync('public/upload')
} catch {}

if (servConfig.cache.refreshOnStart)
{
    CacheEngine.refresh()
    .then((c) => {
      //console.log(c)
      console.log('Database ready')
      server.listen(servConfig.server.port, () => {
        console.log(colors.green("Server now listening on port: ") + colors.yellow(servConfig.server.port));
      });
    })
    .catch((c) => {
      //console.log(c)
    })
}
else
{
  server.listen(servConfig.server.port, () => {
    console.log(colors.green("Server now listening on port: ") + colors.yellow(servConfig.server.port));
  });
}

setInterval(() => {
    CacheEngine.refresh()
    .then((c) => {
      console.log('Database updated')
    })
    .catch((c) => {
    })
}, 1000 * 60 * 60 * 24)

//Middlewares
app.set('view engine', 'ejs');
app.use(cookieSession(servConfig.server.sessions));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(passport.initialize());
app.use(passport.session());

//Http routes
app.use('/' + torrentConfig.downloadPath, express.static('video'));
app.use(express.static(__dirname + '/public'));
app.use('/auth', require('./Routes/auth-routes.js'));
app.use('/profile', require('./Routes/profile-routes.js'));
app.use('/settings', require('./Routes/settings-routes.js'));
app.use('/movie', require('./Routes/video-routes.js'));
app.use('/comments', require('./Routes/comments-routes.js'));
app.use('/search', require('./Routes/search-routes.js'));
app.use('/', require('./Routes/register-routes.js'));
app.use('/',  require('./Routes/home-routes.js'));

//Ws routes
require('./Routes/ws.js')(io);

//Error pages
app.use(function(req, res){
    res.status(404);
    res.render('404', {user: req.user, query: ''});
});
